import 'package:delivery_app/data/models/product_model.dart';
import 'package:delivery_app/widgets/image_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FavorisItem extends StatelessWidget {
  final ProductModel productModel;
  final Function() onDeletePressed;

  const FavorisItem({
    Key? key,
    required this.productModel,
    required this.onDeletePressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width * 0.7,
      decoration: BoxDecoration(
        border: Border.all(
          width: 0.5,
          color: Colors.grey[600]!,
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(
          vertical: 5,
          horizontal: 10,
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            ImageWidget(
              base64: productModel.img,
              size: 60,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    productModel.nomProd,
                    style: const TextStyle(fontWeight: FontWeight.bold),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Text('${productModel.prix.toStringAsFixed(2)} TND'),
                ],
              ),
            ),
            InkWell(
              onTap: onDeletePressed,
              child: const Icon(
                CupertinoIcons.heart_fill,
                color: Colors.red,
              ),
            )
          ],
        ),
      ),
    );
  }
}
