import 'package:delivery_app/widgets/shimmer_loading.dart';
import 'package:flutter/material.dart';

class LoadingImageHeaderShimmer extends StatelessWidget {
  const LoadingImageHeaderShimmer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ShimmerLoading(
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: 200,
        color: Colors.white,
      ),
    );
  }
}
