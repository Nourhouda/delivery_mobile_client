import 'package:delivery_app/utils/extensions.dart';
import 'package:delivery_app/views/search/search_screen_controller.dart';
import 'package:delivery_app/widgets/loading_popup.dart';
import 'package:delivery_app/widgets/product_item.dart';
import 'package:delivery_app/widgets/screen_title_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../widgets/app_button.dart';
import '../../widgets/app_text_fields.dart';

class SearchScreen extends StatefulWidget {
  const SearchScreen({Key? key}) : super(key: key);

  @override
  State<SearchScreen> createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  late SearchScreenController screenController;

  @override
  void initState() {
    super.initState();
    screenController = Provider.of<SearchScreenController>(
      context,
      listen: false,
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        color: Colors.white,
        child: Column(
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const ScreenTitleWidget(
                  subtitle: 'Recherchez vos produits',
                  title: 'Recherche',
                ),
                const SizedBox(
                  height: 20,
                ),
                AppTextField(
                  placeholder: 'Aa...',
                  controller: screenController.searchTextController,
                ),
                const SizedBox(
                  height: 10,
                ),
                AppButton(
                  buttonText: 'Filtrer',
                  onTap: () async {
                    if (screenController.searchTextController.text.isEmpty) {
                      context.displayErrorSnackbar(
                          'Veuillez renseignez le champs recherche');
                    } else {
                      const LoadingPopup().show(context);
                      await screenController.getListProducts();
                      Navigator.pop(context);
                    }
                  },
                ),
              ],
            ),
            Consumer<SearchScreenController>(
              builder: (context, provider, child) {
                return provider.products.isNotEmpty
                    ? Expanded(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                            horizontal: 10,
                            vertical: 20,
                          ),
                          child: GridView.count(
                            crossAxisCount: 3,
                            mainAxisSpacing: 10,
                            crossAxisSpacing: 20,
                            childAspectRatio: 0.7,
                            children: List.generate(
                              provider.products.length,
                              (index) => InkWell(
                                onTap: () {
                                  Navigator.pushNamed(
                                    context,
                                    '/product-detail',
                                    arguments: provider.products[index],
                                  );
                                },
                                child: ProductItem(
                                  product: provider.products[index],
                                ),
                              ),
                            ),
                          ),
                        ),
                      )
                    : provider.searchTextController.text.isEmpty
                        ? Expanded(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Image.asset(
                                  'assets/icons/seach_illustration.png',
                                  width: 200,
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                const Text('Filtrer vos produits')
                              ],
                            ),
                          )
                        : Expanded(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Image.asset(
                                  'assets/icons/seach_illustration.png',
                                  width: 200,
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                const Text('Aucun produit est trouvé')
                              ],
                            ),
                          );
              },
            )
          ],
        ),
      ),
    );
  }
}
