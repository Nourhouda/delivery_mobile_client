import 'package:delivery_app/data/models/product_model.dart';
import 'package:delivery_app/data/models/store_model.dart';
import 'package:delivery_app/data/repositories/product_repository.dart';
import 'package:delivery_app/data/repositories/store_repository.dart';
import 'package:flutter/cupertino.dart';

class StoreDetailsDataProvider extends ChangeNotifier {
  
  late StoreModel store;
  late String vendeurId;

  bool loadingStatus = true;
  List<ProductModel> products = [];

  void resetState(StoreModel store){
    this.store = store;
    vendeurId = '';
    loadingStatus = true;
    getProducts();
  }

  void getProducts() async {
    vendeurId = await StoreRepository().getVendeurIdByStore(store.id) ?? '';
    products = await ProductRepository().getProductsByStore(vendeurId);
    loadingStatus = false;
    notifyListeners();
  }

}