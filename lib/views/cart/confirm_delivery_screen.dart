import 'package:delivery_app/const/shared_prefs_constants.dart';
import 'package:delivery_app/utils/extensions.dart';
import 'package:delivery_app/views/cart/order_confirmation_controller.dart';
import 'package:delivery_app/widgets/app_button.dart';
import 'package:delivery_app/widgets/app_text_fields.dart';
import 'package:delivery_app/widgets/loading_popup.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ConfirmDeliveryScreen extends StatefulWidget {
  const ConfirmDeliveryScreen({Key? key}) : super(key: key);

  @override
  State<ConfirmDeliveryScreen> createState() => _ConfirmDeliveryScreenState();
}

class _ConfirmDeliveryScreenState extends State<ConfirmDeliveryScreen> {
  late OrderConfirmationController confirmationController;

  @override
  void initState() {
    super.initState();
    confirmationController = Provider.of<OrderConfirmationController>(
      context,
      listen: false,
    );
    confirmationController.resetState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        title: const Text('Confirmation de commande'),
      ),
      body: Container(
        color: Colors.white,
        width: MediaQuery.of(context).size.width,
        child: Padding(
          padding: const EdgeInsets.symmetric(
            vertical: 20,
            horizontal: 10,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const Text(
                'Veuillez saisir tout les information pour confirmer votre commande',
                textAlign: TextAlign.center,
              ),
              const SizedBox(
                height: 20,
              ),
              Expanded(
                child: ListView(
                  children: [
                    Column(
                      children: [
                        AppTextField(
                          placeholder: 'Nom',
                          controller: confirmationController.nameTextController,
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        AppTextField(
                          placeholder: 'Prénom',
                          controller:
                              confirmationController.lastnameTextController,
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        AppTextField(
                          placeholder: 'Adresse',
                          controller:
                              confirmationController.adressTextController,
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        AppTextField(
                          placeholder: 'Ville',
                          controller: confirmationController.cityTextController,
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        AppTextField(
                          placeholder: 'Numéro téléphone',
                          controller:
                              confirmationController.phoneTextController,
                        ),
                        const SizedBox(
                          height: 30,
                        ),
                        AppButton(
                          buttonText: 'Confirmer',
                          onTap: () {
                            if (confirmationController.verifyInput()) {
                              const LoadingPopup().show(context);
                              confirmationController
                                  .onSubmitPressed(_confirmationResultHandler);
                            } else {
                              context.displayErrorSnackbar(
                                  'Veuillez remplir tout les champs nécessaire');
                            }
                          },
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _confirmationResultHandler(bool result) async {
    Navigator.pop(context);
    if (result) {
      var sharedPrefs = await SharedPreferences.getInstance();
      await sharedPrefs.remove(cartSharedKey);
      context.displaySnackbar(
        'Commande confirmé',
        Colors.blue,
      );
      Navigator.pop(context);
    } else {
      context.displayErrorSnackbar(
          'Erreur est suvenue lors de la confirmation de commande');
    }
  }
}
