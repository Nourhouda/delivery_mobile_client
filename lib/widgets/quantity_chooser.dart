import 'dart:ffi';

import 'package:delivery_app/widgets/app_button.dart';
import 'package:flutter/material.dart';

class QuantityChooser extends StatefulWidget {
  final Function(int) onQuantityChanged;
  final int initialQuantity;
  final int? maxQuantity;

  const QuantityChooser({
    Key? key,
    required this.onQuantityChanged,
    this.initialQuantity = 1,
    this.maxQuantity,
  }) : super(key: key);

  @override
  State<QuantityChooser> createState() => _QuantityChooserState();
}

class _QuantityChooserState extends State<QuantityChooser> {
  late int quantity = widget.initialQuantity;

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        SizedBox(
          width: 40,
          child: AppButton(
            color: quantity > 1
                ? Theme.of(context).primaryColor
                : Theme.of(context).primaryColor.withOpacity(0.5),
            buttonText: '-',
            onTap: () {
              if (quantity > 1) {
                setState(() {
                  quantity -= 1;
                  widget.onQuantityChanged.call(quantity);
                });
              }
            },
          ),
        ),
        const SizedBox(
          width: 20,
        ),
        Text(quantity.toString()),
        const SizedBox(
          width: 20,
        ),
        SizedBox(
          width: 40,
          child: AppButton(
            buttonText: '+',
            color: widget.maxQuantity == null
                ? Theme.of(context).primaryColor
                : quantity < widget.maxQuantity!
                    ? Theme.of(context).primaryColor
                    : Theme.of(context).primaryColor.withOpacity(0.5),
            onTap: () {
              if (widget.maxQuantity == null ||
                  (quantity < widget.maxQuantity!)) {
                setState(() {
                  quantity += 1;
                  widget.onQuantityChanged.call(quantity);
                });
              }
            },
          ),
        ),
      ],
    );
  }
}
