import 'dart:convert';

import 'package:delivery_app/const/shared_prefs_constants.dart';
import 'package:delivery_app/data/models/reclamation_model.dart';
import 'package:delivery_app/data/models/user_model.dart';
import 'package:delivery_app/data/repositories/app_repository.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ReclamationRepository extends AppRepository {
  Future<ReclamationModel?> createNewReclamation(
    String titre,
    String description,
    String? sellerId,
  ) async {
    try {
      var sharedPrefs = (await SharedPreferences.getInstance());
      var user = UserModel.fromJson(
        jsonDecode((sharedPrefs.getString(userKey)) ?? ''),
      );
      var dataToSend = {
        "titre": titre,
        "description": description,
        "type": "systeme",
        "idClient": user.id,
        "status": "new",
      };
      if (sellerId != null) {
        dataToSend['idVendeur'] = sellerId;
      }
      var serverReponse =
          await dio.post('/api/v1/add-reclamation', data: dataToSend);
      return ReclamationModel.fromJson(serverReponse.data['data']);
    } catch (e) {
      return null;
    }
  }
}
