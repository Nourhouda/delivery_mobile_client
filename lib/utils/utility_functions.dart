import 'package:delivery_app/const/shared_prefs_constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<bool> isUserConnected() async {
  var sharedPrefs =  await SharedPreferences.getInstance();
  return sharedPrefs.getString(emailKey) != null;
}