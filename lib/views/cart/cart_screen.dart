import 'package:delivery_app/views/cart/cart_screen_controller.dart';
import 'package:delivery_app/widgets/app_button.dart';
import 'package:delivery_app/widgets/cart_product_item.dart';
import 'package:delivery_app/widgets/screen_title_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CartScreen extends StatefulWidget {
  const CartScreen({Key? key}) : super(key: key);

  @override
  State<CartScreen> createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {
  late CartScreenController screenController;

  @override
  void initState() {
    super.initState();
    screenController = Provider.of<CartScreenController>(
      context,
      listen: false,
    );
    screenController.getListOfProductsInCart();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Consumer<CartScreenController>(
        builder: (context, provider, child) {
          return Container(
            width: MediaQuery.of(context).size.width,
            color: Colors.white,
            child: provider.productsInCart.isEmpty
                ? Column(
                    children: [
                      const ScreenTitleWidget(
                        title: 'Mon Panier',
                        subtitle: 'Vos produits selectionnés',
                      ),
                      Expanded(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Image.asset(
                              'assets/icons/empty_cart_ill.png',
                              width: 200,
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            const Text(
                                'Aucun produit est trouvé dans le panier')
                          ],
                        ),
                      ),
                    ],
                  )
                : Column(
                    children: [
                      const ScreenTitleWidget(
                        title: 'Mon Panier',
                        subtitle: 'Vos produits selectionnés',
                      ),
                      Expanded(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Expanded(
                              child: ListView.builder(
                                itemCount: provider.productsInCart.length,
                                itemBuilder: (context, index) {
                                  return Padding(
                                    padding: const EdgeInsets.symmetric(
                                      vertical: 10,
                                    ),
                                    child: CartProductItem(
                                      cartModel: provider.productsInCart[index],
                                      onProductChanged:
                                          provider.onProductChanged,
                                      removeProductFromCart: (product) {
                                        showDialog(
                                          context: context,
                                          builder: (context) {
                                            return AlertDialog(
                                              title: const Text('Mon panier'),
                                              content: const Text(
                                                'Voulez-vous vraiment enlever ce produit de panier',
                                              ),
                                              actions: [
                                                TextButton(
                                                  onPressed: () {
                                                    Navigator.pop(context);
                                                  },
                                                  child: const Text('Annuler'),
                                                ),
                                                ElevatedButton(
                                                  onPressed: () {
                                                    provider.onProductRemoved(
                                                        product);
                                                    Navigator.pop(context);
                                                  },
                                                  child:
                                                      const Text('Supprimer'),
                                                ),
                                              ],
                                            );
                                          },
                                        );
                                      },
                                    ),
                                  );
                                },
                              ),
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                            Container(
                              decoration:
                                  BoxDecoration(color: Colors.grey[200]),
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                  vertical: 30,
                                  horizontal: 10,
                                ),
                                child: Row(
                                  children: [
                                    Expanded(
                                      flex: 2,
                                      child: Padding(
                                        padding: const EdgeInsets.symmetric(
                                          horizontal: 20,
                                        ),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              'Total',
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .subtitle1,
                                            ),
                                            Text(
                                              '${provider.totalAmount.toStringAsFixed(2)} DT',
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .subtitle1
                                                  ?.copyWith(
                                                    fontWeight: FontWeight.bold,
                                                  ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      child: AppButton(
                                        buttonText: 'Confirmer',
                                        onTap: () {
                                          Navigator.pushNamed(
                                                  context, '/confirm-order')
                                              .whenComplete(() {
                                            screenController
                                                .getListOfProductsInCart();
                                          });
                                        },
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
          );
        },
      ),
    );
  }
}
