import 'dart:math';
import 'package:delivery_app/data/models/product_model.dart';
import 'package:delivery_app/utils/utility_functions.dart';
import 'package:delivery_app/views/products/product_detail_controller.dart';
import 'package:delivery_app/widgets/app_button.dart';
import 'package:delivery_app/widgets/image_widget.dart';
import 'package:delivery_app/widgets/product_disponibility.dart';
import 'package:delivery_app/widgets/quantity_chooser.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ProductDetailScreen extends StatefulWidget {
  final ProductModel productModel;
  const ProductDetailScreen({
    Key? key,
    required this.productModel,
  }) : super(key: key);

  @override
  State<ProductDetailScreen> createState() => _ProductDetailScreenState();
}

class _ProductDetailScreenState extends State<ProductDetailScreen> {
  late ProductDetailController productDetailController;
  double rating = 0.0;

  bool isConnected = false;

  @override
  void initState() {
    super.initState();
    productDetailController = Provider.of<ProductDetailController>(
      context,
      listen: false,
    );
    rating = (Random().nextDouble() + (Random().nextInt(4)));
    productDetailController.initState(widget.productModel, context);
    isUserConnected().then((value) {
      setState(() {
        isConnected = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        title: Text(widget.productModel.nomProd),
      ),
      body: Consumer<ProductDetailController>(
        builder: (context, provider, child) {
          return Container(
            color: Colors.white,
            width: MediaQuery.of(context).size.width,
            child: Column(
              children: [
                const SizedBox(
                  height: 10,
                ),
                Hero(
                  tag: widget.productModel.id,
                  child: ImageWidget(
                    base64: widget.productModel.img,
                    size: 120,
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      'assets/icons/shop.png',
                      width: 40,
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    Row(
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              widget.productModel.nomProd,
                              style:
                                  const TextStyle(fontWeight: FontWeight.bold),
                            ),
                            // Text(widget.productModel.description),
                            Row(
                              children: [
                                const Icon(Icons.star),
                                Text(
                                  rating.toStringAsFixed(1),
                                ),
                              ],
                            ),
                          ],
                        ),
                        const SizedBox(
                          width: 20,
                        ),
                        ProductDisponibility(
                          quantity: provider.product.quantite,
                        ),
                      ],
                    )
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                const Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: 20,
                    vertical: 10,
                  ),
                ),
                Text(
                  widget.productModel.description,
                  textAlign: TextAlign.start,
                ),
                const SizedBox(
                  height: 30,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 20,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text('Prix:'),
                      Text(
                        '${widget.productModel.prix} DT',
                        style: const TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 20,
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 30,
                ),
                if (provider.product.quantite != 0)
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      QuantityChooser(
                        onQuantityChanged: provider.updateQuantity,
                        maxQuantity: provider.product.quantite,
                      ),
                    ],
                  ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                      vertical: 20,
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        AppButton(
                          buttonText: !provider.inFavorite
                              ? 'Ajouter aux Favoris'
                              : 'Supprimer des favoris',
                          onTap: () {
                            if (isConnected) {
                              !provider.inFavorite
                                  ? provider.addToFavorites.call()
                                  : provider.removeFromFavorite.call();
                            } else {
                              Navigator.pushNamed(context, '/login');
                            }
                          },
                          color: !provider.inFavorite
                              ? Colors.blue[100]!
                              : Colors.red[200]!,
                          icon: !provider.inFavorite
                              ? CupertinoIcons.heart
                              : CupertinoIcons.heart_fill,
                          iconColor: Colors.red,
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        if (provider.product.quantite != 0)
                          AppButton(
                            buttonText: provider.inCart
                                ? 'Supprimer du panier'
                                : 'Ajouter au panier',
                            onTap: () {
                              if (isConnected) {
                                provider.inCart
                                    ? provider.removeFromCart.call()
                                    : provider.addToCart.call();
                              } else {
                                Navigator.pushNamed(context, '/login');
                              }
                            },
                            icon: provider.inCart ? Icons.delete : Icons.add,
                            iconColor: Colors.white,
                          )
                      ],
                    ),
                  ),
                )
              ],
            ),
          );
        },
      ),
    );
  }
}
