import 'package:delivery_app/data/models/product_model.dart';
import 'package:delivery_app/data/repositories/product_repository.dart';
import 'package:flutter/cupertino.dart';

class ListProductController extends ChangeNotifier {
  List<ProductModel> products = [];
  bool dataIsLoading = true;
  String? categoryId;

  resetState(String? category) {
    products = [];
    dataIsLoading = true;
    categoryId = category;
    getListProducts();
  }

  void getListProducts() {
    ProductRepository().getListProducts().then((value) {
      products = value;
      if (categoryId != null && products.isNotEmpty) {
        products = products
            .where(
              (element) => element.idCat == categoryId,
            )
            .toList();
      }
      dataIsLoading = false;
      notifyListeners();
    });
  }
}
