import 'package:delivery_app/data/models/reclamation_model.dart';
import 'package:delivery_app/data/repositories/reclamation_repository.dart';
import 'package:flutter/cupertino.dart';

class ContactScreenController {
  final TextEditingController titreController = TextEditingController();
  final TextEditingController descriptionController = TextEditingController();

  bool verifInput() {
    return titreController.text.isNotEmpty &&
        descriptionController.text.isNotEmpty;
  }

  Future<ReclamationModel?> addNewReclamation(String? sellerId) async {
    return ReclamationRepository().createNewReclamation(
      titreController.text,
      descriptionController.text,
      sellerId,
    );
  }
}
