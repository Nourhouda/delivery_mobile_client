import 'package:delivery_app/data/models/store_model.dart';
import 'package:delivery_app/data/models/vendeur.dart';
import 'package:delivery_app/data/repositories/app_repository.dart';

class StoreRepository extends AppRepository {
  Future<List<StoreModel>> getListStores() async {
    try {
      var serverResponse = await dio.get('/api/v1/all-boutique');
      var data = serverResponse.data['data'];
      return List<StoreModel>.from(data.map((e) => StoreModel.fromJson(e)));
    } catch (e) {
      return [];
    }
  }

  Future<StoreModel?> getStoreBySeller(String sellerId) async {
    try {
      var serverResponse = await dio.get('/api/v1/vendeur/$sellerId');
      var seller = serverResponse.data['data'];
      var storeRequestResponse =
          await dio.get('/api/v1/boutique/${seller['idBoutique']}');
      return StoreModel.fromJson(storeRequestResponse.data['data']);
    } catch (e) {
      return null;
    }
  }

  Future<String?> getVendeurIdByStore(String storeId) async {
    try {
      var serverResponse = await dio.get('/api/v1/all-vendeur');
      var listVendeurs = List<VendeurModel>.from(serverResponse.data['data'].map((e)=>VendeurModel.fromJson(e)));
      return listVendeurs.firstWhere((element) => element.idBoutique == storeId).id;
    } catch (e) {
      return null;
    }
  }
}
