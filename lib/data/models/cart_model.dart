import 'package:delivery_app/data/models/product_model.dart';

class CartModel {
  late int quantity;
  late ProductModel product;

  CartModel(this.product, this.quantity);

  CartModel.fromJson(Map<String, dynamic> json) {
    quantity = json['quantity'];
    product = ProductModel.fromJson(json['product']);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['quantity'] = quantity;
    data['product'] = product;
    return data;
  }
}
