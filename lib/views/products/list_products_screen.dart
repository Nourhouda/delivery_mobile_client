import 'package:delivery_app/views/products/list_product_controller.dart';
import 'package:delivery_app/widgets/loading_widget.dart';
import 'package:delivery_app/widgets/product_item.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ListProductScreen extends StatefulWidget {
  final String? categoryId;

  const ListProductScreen({
    Key? key,
    this.categoryId,
  }) : super(key: key);

  @override
  State<ListProductScreen> createState() => _ListProductScreenState();
}

class _ListProductScreenState extends State<ListProductScreen> {
  late ListProductController controller;

  @override
  void initState() {
    super.initState();
    controller = Provider.of<ListProductController>(
      context,
      listen: false,
    );
    controller.resetState(widget.categoryId);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        title: const Text('Liste des produits'),
      ),
      body: Consumer<ListProductController>(
        builder: (context, provider, child) {
          return Container(
            width: MediaQuery.of(context).size.width,
            color: Colors.white,
            child: provider.dataIsLoading
                ? const LoadingWidget()
                : provider.products.isEmpty
                    ? Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Image.asset(
                            'assets/icons/empty_cart_ill.png',
                            width: 300,
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          const Text('Aucun produit est trouvé')
                        ],
                      )
                    : Column(
                        children: [
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                horizontal: 5,
                              ),
                              child: GridView.count(
                                crossAxisCount: 3,
                                mainAxisSpacing: 10,
                                crossAxisSpacing: 20,
                                childAspectRatio: 0.7,
                                children: List.generate(
                                  provider.products.length,
                                  (index) => InkWell(
                                    onTap: () {
                                      Navigator.pushNamed(
                                        context,
                                        '/product-detail',
                                        arguments: provider.products[index],
                                      );
                                    },
                                    child: ProductItem(
                                      product: provider.products[index],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
          );
        },
      ),
    );
  }
}
