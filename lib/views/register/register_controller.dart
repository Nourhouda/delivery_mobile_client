import 'package:delivery_app/data/models/user_model.dart';
import 'package:delivery_app/data/repositories/auth_repository.dart';
import 'package:flutter/cupertino.dart';

class RegisterController {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController lastNameController = TextEditingController();

  bool checkFormValidity() {
    return emailController.text.isEmpty ||
        passwordController.text.isEmpty ||
        nameController.text.isEmpty ||
        lastNameController.text.isEmpty;
  }

  Future<UserModel?> createUser() {
    return AuthRepository().registerUser(
      emailController.text,
      passwordController.text,
      nameController.text,
      lastNameController.text,
    );
  }
}
