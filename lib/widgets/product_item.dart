// import 'dart:math';
// import 'package:delivery_app/data/models/product_model.dart';
// import 'package:delivery_app/widgets/image_widget.dart';
// import 'package:flutter/material.dart';

// class ProductItem extends StatelessWidget {
//   final ProductModel product;
//   const ProductItem({
//     Key? key,
//     required this.product,
//   }) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Column(
//       crossAxisAlignment: CrossAxisAlignment.start,
//       children: [
//         SizedBox(
//           width: 90,
//           height: 90,
//           child: Stack(
//             children: [
//               Align(
//                 alignment: Alignment.center,
//                 child: Hero(
//                   tag: product.id,
//                   child: ImageWidget(
//                     base64: product.img,
//                   ),
//                 ),
//               ),
//               Align(
//                 alignment: Alignment.bottomRight,
//                 child: Container(
//                   width: 50,
//                   height: 20,
//                   decoration: const BoxDecoration(
//                     color: Colors.red,
//                     borderRadius: BorderRadius.only(
//                       topLeft: Radius.circular(20),
//                     ),
//                   ),
//                   child: Row(
//                     mainAxisAlignment: MainAxisAlignment.center,
//                     children: [
//                       Text(
//                         (Random().nextDouble() + (Random().nextInt(4)))
//                             .toStringAsFixed(1),
//                         style: const TextStyle(
//                           color: Colors.white,
//                           fontSize: 12,
//                         ),
//                       ),
//                       const Icon(
//                         Icons.star,
//                         color: Colors.white,
//                         size: 15,
//                       )
//                     ],
//                   ),
//                 ),
//               ),
//             ],
//           ),
//         ),
//         SizedBox(
//           width: 150,
//           child: Text(
//             product.nomProd,
//             style: const TextStyle(fontWeight: FontWeight.bold),
//           ),
//         ),
//         SizedBox(
//           width: 150,
//           child: Text(
//             '${product.prix} DT',
//             maxLines: 2,
//             textAlign: TextAlign.start,
//           ),
//         ),
//       ],
//     );
//   }
// }
import 'dart:math';
import 'package:delivery_app/data/models/product_model.dart';
import 'package:delivery_app/widgets/image_widget.dart';
import 'package:flutter/material.dart';

class ProductItem extends StatelessWidget {
  final ProductModel product;
  const ProductItem({
    Key? key,
    required this.product,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
          child: SizedBox(
            width: 90,
            height: 90,
            child: Stack(
              children: [
                Align(
                  alignment: Alignment.center,
                  child: Hero(
                    tag: product.id,
                    child: ImageWidget(
                      base64: product.img,
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.bottomRight,
                  child: Container(
                    width: 50,
                    height: 20,
                    decoration: const BoxDecoration(
                      color: Colors.red,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20),
                      ),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          (Random().nextDouble() + (Random().nextInt(4)))
                              .toStringAsFixed(1),
                          style: const TextStyle(
                            color: Colors.white,
                            fontSize: 12,
                          ),
                        ),
                        const Icon(
                          Icons.star,
                          color: Colors.white,
                          size: 15,
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        SizedBox(
          width: 150,
          child: Text(
            product.nomProd,
            style: const TextStyle(fontWeight: FontWeight.bold),
          ),
        ),
        SizedBox(
          width: 150,
          child: Text(
            '${product.prix} DT',
            maxLines: 2,
            textAlign: TextAlign.start,
          ),
        ),
      ],
    );
  }
}
