import 'package:delivery_app/data/models/category_model.dart';
import 'package:delivery_app/widgets/image_widget.dart';
import 'package:flutter/material.dart';

class CategoriesListView extends StatelessWidget {
  final List<CategoryModel> categories;
  final Function(CategoryModel) onCategoryPressed;

  const CategoriesListView({
    Key? key,
    required this.categories,
    required this.onCategoryPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 80,
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: categories
              .map(
                (e) => Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 10,
                  ),
                  child: InkWell(
                    onTap: () {
                      onCategoryPressed.call(e);
                    },
                    child: _CategoryItem(category: e),
                  ),
                ),
              )
              .toList(),
        ),
      ),
    );
  }
}

class _CategoryItem extends StatelessWidget {
  final CategoryModel category;

  const _CategoryItem({
    Key? key,
    required this.category,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          width: 40,
          height: 40,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(5),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 1,
                blurRadius: 1,
                offset: const Offset(0, 1), // changes position of shadow
              ),
            ],
          ),
          child: Center(
            child: ImageWidget(
              base64: category.icon,
              size: 30,
            ),
          ),
        ),
        const SizedBox(
          height: 5,
        ),
        Text(category.nomCat)
      ],
    );
  }
}
