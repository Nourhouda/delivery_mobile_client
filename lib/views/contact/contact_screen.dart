import 'package:delivery_app/utils/extensions.dart';
import 'package:delivery_app/views/contact/contact_screen_controller.dart';
import 'package:delivery_app/widgets/app_button.dart';
import 'package:delivery_app/widgets/app_text_fields.dart';
import 'package:delivery_app/widgets/loading_popup.dart';
import 'package:flutter/material.dart';

class ContactScreen extends StatefulWidget {
  final String? sellerId;

  const ContactScreen({
    Key? key,
    this.sellerId,
  }) : super(key: key);

  @override
  State<ContactScreen> createState() => _ContactScreenState();
}

class _ContactScreenState extends State<ContactScreen> {
  late ContactScreenController screenController;

  @override
  void initState() {
    super.initState();
    screenController = ContactScreenController();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        title: const Text('Réclamation'),
        leading: InkWell(
          child: const Icon(Icons.arrow_back),
          onTap: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: Container(
        color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 10,
            vertical: 20,
          ),
          child: ListView(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Réclamation',
                        style: Theme.of(context).textTheme.headline5,
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Text(
                        widget.sellerId == null
                            ? 'Toute réclamation sera envoyé à notre admin afin de la résoudre'
                            : 'Votre réclamtion sera envoyé au boutique pour la résoudre',
                        style: Theme.of(context).textTheme.caption,
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  AppTextField(
                    placeholder: 'Titre',
                    controller: screenController.titreController,
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  AppTextField(
                    placeholder: 'Description',
                    controller: screenController.descriptionController,
                    height: 200,
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  AppButton(
                    buttonText: 'Envoyer',
                    onTap: _onSubmit,
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _onSubmit() async {
    if (screenController.verifInput()) {
      const LoadingPopup().show(context);
      await screenController.addNewReclamation(widget.sellerId);
      context.displaySnackbar(
        'Réclamation envoyé',
        Colors.green,
      );
      Navigator.pop(context);
      Navigator.pop(context);
    } else {
      context.displayErrorSnackbar('Veuillez saisir tout les champs');
    }
  }
}
