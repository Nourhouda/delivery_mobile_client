// import 'dart:math';

// import 'package:delivery_app/views/home/home_screen_controller.dart';
// import 'package:delivery_app/widgets/categories_list.dart';
// import 'package:delivery_app/widgets/error_data_widget.dart';
// import 'package:delivery_app/widgets/image_widget.dart';
// import 'package:delivery_app/widgets/loading_widget.dart';
// import 'package:delivery_app/widgets/product_item.dart';
// import 'package:delivery_app/widgets/screen_title_widget.dart';
// import 'package:delivery_app/widgets/store_grid_item.dart';
// import 'package:flutter/material.dart';
// import 'package:provider/provider.dart';
// import 'package:staggered_grid_view_flutter/widgets/staggered_grid_view.dart';
// import 'package:staggered_grid_view_flutter/widgets/staggered_tile.dart';

// class HomeScreen extends StatefulWidget {
//   const HomeScreen({Key? key}) : super(key: key);

//   @override
//   State<HomeScreen> createState() => _HomeScreenState();
// }

// class _HomeScreenState extends State<HomeScreen> {
//   late HomeScreenController screenController;

//   @override
//   void initState() {
//     super.initState();
//     screenController = Provider.of<HomeScreenController>(
//       context,
//       listen: false,
//     );
//     WidgetsBinding.instance!.addPostFrameCallback((_) {
//       screenController.initState();
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     List<String> brands = [
//       'https://upload.wikimedia.org/wikipedia/commons/thumb/e/e5/Dr._Oetker-Logo.svg/2560px-Dr._Oetker-Logo.svg.png',
//       'https://upload.wikimedia.org/wikipedia/fr/e/ed/Morris_Garage_logo.png',
//       'https://upload.wikimedia.org/wikipedia/commons/thumb/5/5b/Carrefour_logo.svg/800px-Carrefour_logo.svg.png',
//       'http://assets.stickpng.com/images/5a1c2d20f65d84088faf13c4.png',
//       'https://www.keejob.com/media/recruiter/recruiter_18845/logo-18845-20190911-133102.png',
//     ];
//     return Consumer<HomeScreenController>(
//       builder: (context, provider, child) {
//         return SafeArea(
//           child: Container(
//             width: MediaQuery.of(context).size.width,
//             color: Colors.white,
//             child: provider.dataIsLoading
//                 ? const LoadingWidget()
//                 : Padding(
//                     padding: const EdgeInsets.symmetric(
//                       horizontal: 20,
//                     ),
//                     child: Column(
//                       children: [
//                         const ScreenTitleWidget(
//                           title: 'Accueil',
//                           subtitle: 'Tout les nouveautés',
//                         ),
//                         const SizedBox(
//                           height: 10,
//                         ),
//                         Expanded(
//                           child: Column(
//                             crossAxisAlignment: CrossAxisAlignment.start,
//                             children: [
//                               Text(
//                                 'Catégories',
//                                 style: Theme.of(context).textTheme.caption,
//                               ),
//                               const SizedBox(
//                                 height: 10,
//                               ),
//                               provider.categories.isEmpty
//                                   ? const Expanded(
//                                       child: ErrorDataWidget(),
//                                     )
//                                   : CategoriesListView(
//                                       categories: provider.categories,
//                                       onCategoryPressed: (category) {
//                                         Navigator.pushNamed(
//                                           context,
//                                           '/list-products',
//                                           arguments: category.id,
//                                         );
//                                       },
//                                     ),
//                               const SizedBox(
//                                 height: 10,
//                               ),
//                               Expanded(
//                                 flex: 2,
//                                 child: Column(
//                                   children: [
//                                     Row(
//                                       mainAxisAlignment:
//                                           MainAxisAlignment.spaceBetween,
//                                       children: [
//                                         Text(
//                                           'Nouveaux produits',
//                                           style: Theme.of(context)
//                                               .textTheme
//                                               .caption,
//                                         ),
//                                         InkWell(
//                                           onTap: () {
//                                             Navigator.pushNamed(
//                                               context,
//                                               '/list-products',
//                                             );
//                                           },
//                                           child: const Text(
//                                             'voir tout',
//                                             style: TextStyle(
//                                               decoration:
//                                                   TextDecoration.underline,
//                                               color: Colors.blue,
//                                             ),
//                                           ),
//                                         ),
//                                       ],
//                                     ),
//                                     provider.products.isEmpty
//                                         ? const Expanded(
//                                             child: ErrorDataWidget(),
//                                           )
//                                         : Expanded(
//                                             child: ListView.builder(
//                                               scrollDirection: Axis.horizontal,
//                                               itemCount:
//                                                   provider.products.length > 3
//                                                       ? 3
//                                                       : provider
//                                                           .products.length,
//                                               itemBuilder: (context, index) =>
//                                                   InkWell(
//                                                 onTap: () {
//                                                   Navigator.pushNamed(
//                                                     context,
//                                                     '/product-detail',
//                                                     arguments: provider
//                                                         .products[index],
//                                                   );
//                                                 },
//                                                 child: ProductItem(
//                                                   product:
//                                                       provider.products[index],
//                                                 ),
//                                               ),
//                                             ),
//                                           )
//                                   ],
//                                 ),
//                               ),
//                               Expanded(
//                                 flex: 2,
//                                 child: Column(
//                                   children: [
//                                     Row(
//                                       mainAxisAlignment:
//                                           MainAxisAlignment.spaceBetween,
//                                       children: [
//                                         Text(
//                                           'Nouveaux boutiques',
//                                           style: Theme.of(context)
//                                               .textTheme
//                                               .caption,
//                                         ),
//                                         InkWell(
//                                           onTap: () {
//                                             Navigator.pushNamed(
//                                                 context, '/list-stores');
//                                           },
//                                           child: const Text(
//                                             'voir tout',
//                                             style: TextStyle(
//                                               decoration:
//                                                   TextDecoration.underline,
//                                               color: Colors.blue,
//                                             ),
//                                           ),
//                                         ),
//                                       ],
//                                     ),
//                                     provider.stores.isEmpty
//                                         ? const Expanded(
//                                             child: ErrorDataWidget(),
//                                           )
//                                         : Expanded(
//                                             child: ListView.builder(
//                                               scrollDirection: Axis.horizontal,
//                                               itemCount:
//                                                   provider.stores.length > 3
//                                                       ? 3
//                                                       : provider.stores.length,
//                                               itemBuilder: (context, index) =>
//                                                   StoreGridItem(
//                                                       store: provider
//                                                           .stores[index]),
//                                             ),
//                                           )
//                                   ],
//                                 ),
//                               ),
//                               Expanded(
//                                 child: Column(
//                                   crossAxisAlignment: CrossAxisAlignment.start,
//                                   children: [
//                                     Text(
//                                       'Nos marques',
//                                       style:
//                                           Theme.of(context).textTheme.caption,
//                                     ),
//                                     Expanded(
//                                       child: StaggeredGridView.countBuilder(
//                                         crossAxisCount: 2,
//                                         scrollDirection: Axis.horizontal,
//                                         itemCount: provider.brandImages.length,
//                                         itemBuilder:
//                                             (BuildContext context, int index) {
//                                           return ImageWidget(
//                                             base64: provider.brandImages[index],
//                                             size: 100,
//                                           );
//                                         },
//                                         staggeredTileBuilder: (int index) =>
//                                             StaggeredTile.count(
//                                           2,
//                                           Random().nextInt(30).isEven ? 2 : 1,
//                                         ),
//                                         mainAxisSpacing: 4.0,
//                                         crossAxisSpacing: 4.0,
//                                       ),
//                                     )
//                                   ],
//                                 ),
//                               ),
//                             ],
//                           ),
//                         ),
//                       ],
//                     ),
//                   ),
//           ),
//         );
//       },
//     );
//   }
// }
import 'dart:math';

import 'package:delivery_app/views/home/home_screen_controller.dart';
import 'package:delivery_app/widgets/categories_list.dart';
import 'package:delivery_app/widgets/error_data_widget.dart';
import 'package:delivery_app/widgets/image_widget.dart';
import 'package:delivery_app/widgets/loading_widget.dart';
import 'package:delivery_app/widgets/product_item.dart';
import 'package:delivery_app/widgets/screen_title_widget.dart';
import 'package:delivery_app/widgets/store_grid_item.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:staggered_grid_view_flutter/widgets/staggered_grid_view.dart';
import 'package:staggered_grid_view_flutter/widgets/staggered_tile.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  late HomeScreenController screenController;

  @override
  void initState() {
    super.initState();
    screenController = Provider.of<HomeScreenController>(
      context,
      listen: false,
    );
    WidgetsBinding.instance.addPostFrameCallback((_) {
      screenController.initState();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<HomeScreenController>(
      builder: (context, provider, child) {
        return SizedBox(
          height: MediaQuery.of(context).size.height,
          child: SingleChildScrollView(
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              color: Colors.white,
              child: provider.dataIsLoading
                  ? const LoadingWidget()
                  : Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 10,
                      ),
                      child: Column(
                        children: [
                          const ScreenTitleWidget(
                            title: 'Accueil',
                            subtitle: 'Tout les nouveautés',
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Catégories',
                                  style: Theme.of(context).textTheme.caption,
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                provider.categories.isEmpty
                                    ? const Expanded(
                                        child: ErrorDataWidget(),
                                      )
                                    : CategoriesListView(
                                        categories: provider.categories,
                                        onCategoryPressed: (category) {
                                          Navigator.pushNamed(
                                            context,
                                            '/list-products',
                                            arguments: category.id,
                                          );
                                        },
                                      ),
                                const SizedBox(
                                  height: 10,
                                ),
                                Expanded(
                                  flex: 2,
                                  child: Column(
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            'Nouveaux produits',
                                            style: Theme.of(context)
                                                .textTheme
                                                .caption,
                                          ),
                                          InkWell(
                                            onTap: () {
                                              Navigator.pushNamed(
                                                context,
                                                '/list-products',
                                              );
                                            },
                                            child: const Text(
                                              'voir tout',
                                              style: TextStyle(
                                                decoration:
                                                    TextDecoration.underline,
                                                color: Colors.blue,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      provider.products.isEmpty
                                          ? const Expanded(
                                              child: ErrorDataWidget(),
                                            )
                                          : Expanded(
                                              child: ListView.builder(
                                                scrollDirection:
                                                    Axis.horizontal,
                                                itemCount:
                                                    provider.products.length > 3
                                                        ? 3
                                                        : provider
                                                            .products.length,
                                                itemBuilder: (context, index) =>
                                                    InkWell(
                                                  onTap: () {
                                                    Navigator.pushNamed(
                                                      context,
                                                      '/product-detail',
                                                      arguments: provider
                                                          .products[index],
                                                    );
                                                  },
                                                  child: ProductItem(
                                                    product: provider
                                                        .products[index],
                                                  ),
                                                ),
                                              ),
                                            )
                                    ],
                                  ),
                                ),
                                const SizedBox(
                                  height: 30,
                                ),
                                Expanded(
                                  flex: 2,
                                  child: Column(
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            'Nouveaux boutiques',
                                            style: Theme.of(context)
                                                .textTheme
                                                .caption,
                                          ),
                                          InkWell(
                                            onTap: () {
                                              Navigator.pushNamed(
                                                  context, '/list-stores');
                                            },
                                            child: const Text(
                                              'voir tout',
                                              style: TextStyle(
                                                decoration:
                                                    TextDecoration.underline,
                                                color: Colors.blue,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      provider.stores.isEmpty
                                          ? const Expanded(
                                              child: ErrorDataWidget(),
                                            )
                                          : Expanded(
                                              child: ListView.builder(
                                                scrollDirection:
                                                    Axis.horizontal,
                                                itemCount:
                                                    provider.stores.length > 3
                                                        ? 3
                                                        : provider
                                                            .stores.length,
                                                itemBuilder: (context, index) =>
                                                    StoreGridItem(
                                                        store: provider
                                                            .stores[index]),
                                              ),
                                            )
                                    ],
                                  ),
                                ),
                                const SizedBox(
                                  height: 30,
                                ),
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Nos marques',
                                        style:
                                            Theme.of(context).textTheme.caption,
                                      ),
                                      Expanded(
                                        child: StaggeredGridView.countBuilder(
                                          crossAxisCount: 2,
                                          scrollDirection: Axis.horizontal,
                                          itemCount:
                                              provider.brandImages.length,
                                          itemBuilder: (BuildContext context,
                                              int index) {
                                            return ImageWidget(
                                              base64:
                                                  provider.brandImages[index],
                                              size: 100,
                                            );
                                          },
                                          staggeredTileBuilder: (int index) =>
                                              StaggeredTile.count(
                                            2,
                                            Random().nextInt(30).isEven ? 2 : 1,
                                          ),
                                          mainAxisSpacing: 4.0,
                                          crossAxisSpacing: 4.0,
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
            ),
          ),
        );
      },
    );
  }
}
