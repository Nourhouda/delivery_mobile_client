import 'package:delivery_app/const/arrays.dart';

Map<Categories, String> categoryImages = {
  Categories.children: 'assets/icons/toys.png',
  Categories.clothes: 'assets/icons/tshirt.png',
  Categories.fourniture: 'assets/icons/furnitures.png',
  Categories.hiTech: 'assets/icons/tech.png',
  Categories.phones: 'assets/icons/phone.png',
};
