import 'package:delivery_app/data/models/product_model.dart';
import 'package:delivery_app/data/repositories/app_repository.dart';

class ProductRepository extends AppRepository {
  Future<List<ProductModel>> getListProducts() async {
    try {
      var serverResponse = await dio.get('/api/v1/all-product');
      return List<ProductModel>.from(
        serverResponse.data["data"].map(
          (e) => ProductModel.fromJson(e),
        ),
      );
    } catch (e) {
      return [];
    }
  }

  Future<List<ProductModel>> fitlerListProducts(String query) async {
    try {
      var list = await getListProducts();
      return list
          .where(
            (element) =>
                element.nomProd.toLowerCase().contains(query.toLowerCase()),
          )
          .toList();
    } catch (e) {
      return [];
    }
  }

  Future<List<ProductModel>> getProductsByStore(String storeId) async {
    try {
      var list = await getListProducts();
      return list
          .where(
            (element) => element.idVendeur == storeId,
          )
          .toList();
    } catch (e) {
      return [];
    }
  }
}
