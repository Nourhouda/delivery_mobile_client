import 'package:delivery_app/data/models/order_model.dart';
import 'package:delivery_app/data/models/product_model.dart';
import 'package:delivery_app/data/models/store_model.dart';
import 'package:delivery_app/utils/fcm_service.dart';
import 'package:delivery_app/views/cart/cart_screen_controller.dart';
import 'package:delivery_app/views/cart/confirm_delivery_screen.dart';
import 'package:delivery_app/views/cart/order_confirmation_controller.dart';
import 'package:delivery_app/views/contact/contact_screen.dart';
import 'package:delivery_app/views/container/container_screen.dart';
import 'package:delivery_app/views/favoris/favoris_screen.dart';
import 'package:delivery_app/views/home/home_screen_controller.dart';
import 'package:delivery_app/views/login/login_screen.dart';
import 'package:delivery_app/views/orders/list_order_controller.dart';
import 'package:delivery_app/views/orders/list_orders_screen.dart';
import 'package:delivery_app/views/orders/order_details_screen.dart';
import 'package:delivery_app/views/products/list_product_controller.dart';
import 'package:delivery_app/views/products/list_products_screen.dart';
import 'package:delivery_app/views/products/product_detail_controller.dart';
import 'package:delivery_app/views/products/product_detail_screen.dart';
import 'package:delivery_app/views/profile/profile_info_screen.dart';
import 'package:delivery_app/views/register/register_screen.dart';
import 'package:delivery_app/views/reset_password/reset_password_screen.dart';
import 'package:delivery_app/views/search/search_screen_controller.dart';
import 'package:delivery_app/views/stores/list_stores.dart';
import 'package:delivery_app/views/stores/list_stores_controller.dart';
import 'package:delivery_app/views/stores/store_detail_screen.dart';
import 'package:delivery_app/views/stores/store_details_data_provider.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  ManageNotification.init();

  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (context) => HomeScreenController(),
        ),
        ChangeNotifierProvider(
          create: (context) => ListProductController(),
        ),
        ChangeNotifierProvider(
          create: (context) => CartScreenController(),
        ),
        ChangeNotifierProvider(
          create: (context) => OrderConfirmationController(),
        ),
        ChangeNotifierProvider(
          create: (context) => ProductDetailController(),
        ),
        ChangeNotifierProvider(
          create: (context) => SearchScreenController(),
        ),
        ChangeNotifierProvider(
          create: (context) => ListStoreController(),
        ),
        ChangeNotifierProvider(
          create: (context) => ListOrderController(),
        ),
        ChangeNotifierProvider(
          create: (context) => StoreDetailsDataProvider(),
        ),
      ],
      child: const MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        fontFamily: 'DMSans',
        primarySwatch: Colors.blue,
        primaryColor: const Color(0xff022E63),
      ),
      routes: {
        '/': (context) => const ContainerScreen(),
        '/login': (context) => const LoginScreen(),
        '/register': (context) => const RegisterScreen(),
        '/reset-password': (context) => const ResetPassword(),
        '/home-screen': (context) => const ContainerScreen(),
        '/profile-info': (context) => const ProfileInfoScreen(),
        '/orders': (context) => const ListOrderScreen(),
        '/favoris': (context) => const FavorisScreen(),
        '/list-stores': (context) => const ListStoreScreen(),
        '/confirm-order': (context) => const ConfirmDeliveryScreen(),
      },
      onGenerateRoute: _generateRoutes,
    );
  }

  Route<dynamic> _generateRoutes(settings) {
    if (settings.name == '/product-detail') {
      final args = settings.arguments as ProductModel;
      return MaterialPageRoute(
        builder: (context) {
          return ProductDetailScreen(
            productModel: args,
          );
        },
      );
    } else if (settings.name == '/list-products') {
      final args = settings.arguments as String?;
      return MaterialPageRoute(
        builder: (context) {
          return ListProductScreen(
            categoryId: args,
          );
        },
      );
    } else if (settings.name == '/store-detail') {
      final store = settings.arguments as StoreModel;
      return MaterialPageRoute(
        builder: (context) {
          return StoreDetailScreen(
            storeModel: store,
          );
        },
      );
    } else if (settings.name == '/contact') {
      final seller = settings.arguments as String?;
      return MaterialPageRoute(
        builder: (context) {
          return ContactScreen(
            sellerId: seller,
          );
        },
      );
    } else if (settings.name == '/order-detail') {
      final order = settings.arguments as OrderModel;
      return MaterialPageRoute(
        builder: (context) {
          return OrderDetailScreen(
            orderModel: order,
          );
        },
      );
    } else {
      return MaterialPageRoute(
        builder: (context) {
          return const Scaffold();
        },
      );
    }
  }
}
