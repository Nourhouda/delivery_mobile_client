import 'package:flutter/material.dart';

class AppButton extends StatelessWidget {
  final Color color;
  final String buttonText;
  final Function() onTap;
  final IconData? icon;
  final Color? iconColor;

  const AppButton({
    Key? key,
    this.color = const Color(0xff022E63),
    required this.buttonText,
    required this.onTap,
    this.icon,
    this.iconColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        width: 200,
        height: 40,
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.circular(10),
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 5,
          ),
          child: icon != null
              ? Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      buttonText,
                      style: const TextStyle(color: Colors.white),
                    ),
                    Icon(
                      icon,
                      color: iconColor,
                      size: 20,
                    ),
                  ],
                )
              : Center(
                  child: Text(
                    buttonText,
                    style: const TextStyle(color: Colors.white),
                  ),
                ),
        ),
      ),
    );
  }
}
