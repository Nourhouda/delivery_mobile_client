import 'package:flutter/material.dart';

class ProductDisponibility extends StatelessWidget {
  final int quantity;

  const ProductDisponibility({Key? key, required this.quantity})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return quantity == 0
        ? Container(
            width: 90,
            decoration: BoxDecoration(
              color: Colors.red.withOpacity(0.1),
              border: Border.all(
                color: Colors.red,
                width: 1,
              ),
              borderRadius: BorderRadius.circular(5),
            ),
            child: const Padding(
              padding: EdgeInsets.all(8.0),
              child: Center(
                child: Text(
                  'Hors stock',
                  style: TextStyle(
                    color: Colors.red,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
          )
        : Container(
            width: 90,
            decoration: BoxDecoration(
              color: Colors.green.withOpacity(0.1),
              border: Border.all(
                color: Colors.green,
                width: 1,
              ),
              borderRadius: BorderRadius.circular(5),
            ),
            child: const Padding(
              padding: EdgeInsets.all(8.0),
              child: Center(
                child: Text(
                  'Disponible',
                  style: TextStyle(
                    color: Colors.green,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
          );
  }
}
