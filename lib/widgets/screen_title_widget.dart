// import 'package:flutter/material.dart';

// class ScreenTitleWidget extends StatelessWidget {
//   final String title;
//   final String subtitle;

//   const ScreenTitleWidget({
//     Key? key,
//     required this.subtitle,
//     required this.title,
//   }) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Column(
//       children: [
//         Align(
//           alignment: Alignment.topLeft,
//           child: Padding(
//             padding: const EdgeInsets.only(
//               left: 20,
//               top: 30,
//             ),
//             child: Text(
//               title,
//               style: Theme.of(context).textTheme.headline5?.copyWith(
//                     fontWeight: FontWeight.bold,
//                   ),
//             ),
//           ),
//         ),
//         Align(
//           alignment: Alignment.topLeft,
//           child: Padding(
//             padding: const EdgeInsets.only(left: 20),
//             child: Text(
//               subtitle,
//               style: Theme.of(context).textTheme.subtitle1,
//             ),
//           ),
//         ),
//       ],
//     );
//   }
// }
import 'package:flutter/material.dart';

class ScreenTitleWidget extends StatelessWidget {
  final String title;
  final String subtitle;

  const ScreenTitleWidget({
    Key? key,
    required this.subtitle,
    required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Align(
          alignment: Alignment.topLeft,
          child: Padding(
            padding: const EdgeInsets.only(
              left: 20,
              top: 10,
            ),
            child: Text(
              title,
              style: Theme.of(context).textTheme.headline5?.copyWith(
                    fontWeight: FontWeight.bold,
                  ),
            ),
          ),
        ),
        Align(
          alignment: Alignment.topLeft,
          child: Padding(
            padding: const EdgeInsets.only(left: 20),
            child: Text(
              subtitle,
              style: Theme.of(context).textTheme.subtitle1,
            ),
          ),
        ),
      ],
    );
  }
}
