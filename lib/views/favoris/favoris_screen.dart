import 'dart:convert';

import 'package:delivery_app/const/shared_prefs_constants.dart';
import 'package:delivery_app/data/models/product_model.dart';
import 'package:delivery_app/utils/extensions.dart';
import 'package:delivery_app/widgets/error_data_widget.dart';
import 'package:delivery_app/widgets/favoris_item.dart';
import 'package:flutter/material.dart';
import 'package:searchable_listview/searchable_listview.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FavorisScreen extends StatefulWidget {
  const FavorisScreen({Key? key}) : super(key: key);

  @override
  State<FavorisScreen> createState() => _FavorisScreenState();
}

class _FavorisScreenState extends State<FavorisScreen> {
  List<ProductModel> products = [];

  @override
  void initState() {
    super.initState();
    SharedPreferences.getInstance().then((prefs) {
      var savedFavorites = prefs.getString(favoritesKey);
      if (savedFavorites != null) {
        setState(() {
          products = List<ProductModel>.from(
            jsonDecode(savedFavorites).map(
              (e) => ProductModel.fromJson(e),
            ),
          );
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        title: const Text('Mes favoris'),
        leading: InkWell(
          child: const Icon(Icons.arrow_back),
          onTap: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: Container(
        color: Colors.white,
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 10,
            vertical: 20,
          ),
          child: products.isEmpty
              ? Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      'assets/icons/favorite_illustration.png',
                      width: 250,
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    const Text('Aucun produit est ajouté aux favoris')
                  ],
                )
              : Column(
                  children: [
                    Expanded(
                      child: SearchableList<ProductModel>(
                        inputDecoration: InputDecoration(
                          label: const Text('Recherche'),
                          fillColor: const Color(0xffF3F3F3),
                          filled: true,
                          enabledBorder: OutlineInputBorder(
                            borderSide: const BorderSide(
                              color: Color(0xff61C002),
                            ),
                            borderRadius: BorderRadius.circular(10),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide: const BorderSide(
                              color: Color(0xff61C002),
                            ),
                            borderRadius: BorderRadius.circular(10),
                          ),
                          errorBorder: OutlineInputBorder(
                            borderSide: const BorderSide(
                              color: Color(0xff61C002),
                            ),
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                        initialList: products,
                        emptyWidget: const ErrorDataWidget(),
                        filter: (query) {
                          return products
                              .where(
                                (element) =>
                                    element.nomProd.toLowerCase().contains(
                                          query.toLowerCase(),
                                        ),
                              )
                              .toList();
                        },
                        builder: (item) {
                          return FavorisItem(
                            productModel: item,
                            onDeletePressed: (){
                              _removeFromFavorite(item);
                            },
                          );
                        },
                      ),
                    ),
                  ],
                ),
        ),
      ),
    );
  }

  _removeFromFavorite(ProductModel productModel) async{
    products.removeWhere((e) => e.id == productModel.id);
    (await SharedPreferences.getInstance()).setString(favoritesKey, jsonEncode(products));
    setState(() {});
    context.displaySnackbar('Produit supprimé de la liste des favoris',Colors.blue);
  }
}
