import 'package:delivery_app/data/models/store_model.dart';
import 'package:delivery_app/views/stores/store_details_data_provider.dart';
import 'package:delivery_app/widgets/image_widget.dart';
import 'package:delivery_app/widgets/loading_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../widgets/product_item.dart';

class StoreDetailScreen extends StatefulWidget {
  final StoreModel storeModel;

  const StoreDetailScreen({
    Key? key,
    required this.storeModel,
  }) : super(key: key);

  @override
  State<StoreDetailScreen> createState() => _StoreDetailScreenState();
}

class _StoreDetailScreenState extends State<StoreDetailScreen> {
  late StoreDetailsDataProvider storeDetailsDataProvider;

  @override
  void initState() {
    super.initState();
    storeDetailsDataProvider =
        Provider.of<StoreDetailsDataProvider>(context, listen: false);
    storeDetailsDataProvider.resetState(widget.storeModel);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Détail boutique'),
        backgroundColor: Theme.of(context).primaryColor,
      ),
      body: Container(
        color: Colors.white,
        width: MediaQuery.of(context).size.width,
        child: Stack(
          children: [
            Align(
              alignment: Alignment.topCenter,
              child: ImageWidget(
              base64: widget.storeModel.imgBoutique,
              size: MediaQuery.of(context).size.width,
            ),
            ),
            Align(
              alignment: Alignment.topCenter,
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 150),
                child: Container(
                  height: MediaQuery.of(context).size.height,
                  decoration: const BoxDecoration(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(40),
                      topRight: Radius.circular(40),
                    ),
                    color: Colors.white,
                  ),
                  child: Column(
                    children: [
                      const SizedBox(
                        height: 20,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Image.asset(
                            'assets/icons/shop.png',
                            width: 40,
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                widget.storeModel.nomBoutique,
                                style: const TextStyle(
                                    fontWeight: FontWeight.bold),
                              ),
                              Text(
                                widget.storeModel.adresseSiege.isNotEmpty
                                    ? widget.storeModel.adresseSiege
                                    : 'Adresse: N/A',
                                maxLines: 2,
                                textAlign: TextAlign.start,
                              ),
                              Row(
                                children: const [
                                  Icon(Icons.star),
                                  Text('3.5'),
                                ],
                              ),
                            ],
                          )
                        ],
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Expanded(
                        child: Consumer<StoreDetailsDataProvider>(
                          builder: (context, provider, child) {
                            if (provider.loadingStatus) {
                              return const LoadingWidget();
                            }
                            if (provider.products.isEmpty) {
                              return Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Image.asset(
                                    'assets/icons/empty_order_img.png',
                                    width: 200,
                                  ),
                                  const Text('Aucun produit est trouvé')
                                ],
                              );
                            }
                            return GridView.count(
                              crossAxisCount: 3,
                              mainAxisSpacing: 10,
                              crossAxisSpacing: 20,
                              childAspectRatio: 0.7,
                              children: List.generate(
                                provider.products.length,
                                (index) => InkWell(
                                  onTap: () {
                                    Navigator.pushNamed(
                                      context,
                                      '/product-detail',
                                      arguments: provider.products[index],
                                    );
                                  },
                                  child: ProductItem(
                                    product: provider.products[index],
                                  ),
                                ),
                              ),
                            );
                          },
                        ),
                      )
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
