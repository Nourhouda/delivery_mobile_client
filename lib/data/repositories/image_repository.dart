import 'package:dio/dio.dart';

class ImageRepository {
  Dio dioInstance = Dio();
  ImageRepository() {
    dioInstance.options = BaseOptions(baseUrl: 'https://api.unsplash.com');
  }

  Future<String?> getRandomImage() async {
    try {
      var serverResponse = await dioInstance.get(
        '/photos/random',
        queryParameters: {
          'client_id': '7fzhX1FaWbFSfMBgLRrefRQdqbvC-rXrjBCGuvFXG0k',
          'query': 'store'
        },
      );
      return serverResponse.data['urls']['full'];
    } catch (e) {
      return null;
    }
  }
}
