import 'dart:convert';

import 'package:flutter/material.dart';

class ImageWidget extends StatelessWidget {
  final String base64;
  final double size;

  const ImageWidget({
    Key? key,
    required this.base64,
    this.size = 80,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Image.memory(
      const Base64Decoder().convert(
        base64.substring(
          base64.indexOf(',') + 1,
        ),
      ),
      width: size,
      errorBuilder: (context, error, stackTrace) {
        return Image.asset(
          'assets/icons/picture.png',
          width: size,
        );
      },
    );
  }
}
