import 'package:delivery_app/data/models/store_model.dart';
import 'package:delivery_app/data/repositories/store_repository.dart';
import 'package:flutter/cupertino.dart';

class ListStoreController extends ChangeNotifier {
  List<StoreModel> stores = [];
  bool dataIsLoading = true;

  resetState() {
    stores = [];
    dataIsLoading = true;
    getListOfStores();
  }

  getListOfStores() {
    StoreRepository().getListStores().then((value) {
      stores = value;
      dataIsLoading = false;
      notifyListeners();
    });
  }
}
