import 'dart:convert';

import 'package:delivery_app/const/shared_prefs_constants.dart';
import 'package:delivery_app/data/models/cart_model.dart';
import 'package:delivery_app/data/models/product_model.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProductDetailController extends ChangeNotifier {
  List<ProductModel> productsInFavorite = [];
  List<CartModel> productsInCart = [];

  late ProductModel product;
  late SharedPreferences preferences;
  late BuildContext context;

  bool inFavorite = false;
  bool inCart = false;

  int quantity = 1;

  initState(ProductModel productModel, BuildContext buildContext) async {
    product = productModel;
    preferences = await SharedPreferences.getInstance();
    context = buildContext;

    var savedFavorites = preferences.getString(favoritesKey);
    var savedCart = preferences.getString(cartSharedKey);

    if (savedFavorites != null) {
      productsInFavorite = List<ProductModel>.from(
        jsonDecode(savedFavorites).map(
          (e) => ProductModel.fromJson(e),
        ),
      );
    }
    if (savedCart != null) {
      productsInCart = List<CartModel>.from(
        jsonDecode(savedCart).map(
          (e) => CartModel.fromJson(e),
        ),
      );
    }

    inFavorite = productsInFavorite.any(
      (element) => element.id == productModel.id,
    );
    inCart = productsInCart.any(
      (element) => element.product.id == productModel.id,
    );

    notifyListeners();
  }

  updateQuantity(q) {
    quantity = q;
  }

  addToFavorites() async {
    productsInFavorite.add(product);
    preferences.setString(favoritesKey, jsonEncode(productsInFavorite));
    inFavorite = true;
    notifyListeners();
  }

  removeFromFavorite() async {
    productsInFavorite.removeWhere((e) => e.id == product.id);
    preferences.setString(favoritesKey, jsonEncode(productsInFavorite));
    inFavorite = false;
    notifyListeners();
  }

  addToCart() async {
    if (quantity > 0) {
      productsInCart.add(CartModel(product, quantity));
      preferences.setString(cartSharedKey, jsonEncode(productsInCart));
      inCart = true;
    }
    notifyListeners();
  }

  removeFromCart() async {
    productsInCart.removeWhere((e) => e.product.id == product.id);
    preferences.setString(cartSharedKey, jsonEncode(productsInCart));
    inCart = false;
    notifyListeners();
  }
}
