class ReclamationModel {
  late String id;
  late String titre;
  late String description;
  late String type;
  late String status;

  ReclamationModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    titre = json['titre'];
    description = json['description'];
    type = json['type'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'titre': titre,
      'description': description,
      'type': type,
      'status': status,
    };
  }
}
