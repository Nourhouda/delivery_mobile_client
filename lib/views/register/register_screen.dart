import 'package:delivery_app/utils/extensions.dart';
import 'package:delivery_app/views/register/register_controller.dart';
import 'package:delivery_app/widgets/app_button.dart';
import 'package:delivery_app/widgets/app_text_fields.dart';
import 'package:delivery_app/widgets/loading_popup.dart';
import 'package:flutter/material.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  late RegisterController screenController;

  @override
  void initState() {
    super.initState();
    screenController = RegisterController();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          color: Colors.white,
          width: MediaQuery.of(context).size.width,
          child: Padding(
            padding: const EdgeInsets.all(10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Image.asset(
                      'assets/logo/logo_full.png',
                      width: 100,
                    ),
                    RichText(
                      text: const TextSpan(
                        children: [
                          TextSpan(
                            text: 'Bienvenue',
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 18,
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
                Expanded(
                  child: ListView(
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          const Text(
                            'Créer un compte',
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 18),
                          ),
                          const Text(
                            'Veuillez saisir vos identifiants',
                            style: TextStyle(
                              color: Colors.black,
                            ),
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          AppTextField(
                            placeholder: 'Nom',
                            controller: screenController.nameController,
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          AppTextField(
                            placeholder: 'Prénom',
                            controller: screenController.lastNameController,
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          AppTextField(
                            placeholder: 'Adresse mail',
                            controller: screenController.emailController,
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          AppTextField(
                            placeholder: 'Mot de passe',
                            controller: screenController.passwordController,
                            obscureText: true,
                          ),
                          const SizedBox(
                            height: 40,
                          ),
                          AppButton(
                            buttonText: 'Créer un compte',
                            onTap: _onSubmitPressed,
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          InkWell(
                            onTap: () {
                              Navigator.pop(context);
                            },
                            child: RichText(
                              text: const TextSpan(
                                children: [
                                  TextSpan(
                                    text: "Vous avez déjà un compte ? ",
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 14,
                                    ),
                                  ),
                                  TextSpan(
                                    text: "S'authentifier",
                                    style: TextStyle(
                                      color: Color(0xff61C002),
                                      fontSize: 14,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  _onSubmitPressed() {
    if (screenController.checkFormValidity()) {
      context
          .displayErrorSnackbar("Veuillez saisir tout les champs obligatoires");
    } else {
      const LoadingPopup().show(context);
      screenController.createUser().then((result) {
        Navigator.pop(context);
        if (result != null) {
          Navigator.popUntil(context, ModalRoute.withName('/'));
        } else {
          context.displayErrorSnackbar(
              "Erreur est suvernue lors de la création du compte");
        }
      });
    }
  }
}
