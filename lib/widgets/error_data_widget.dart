import 'package:flutter/material.dart';

class ErrorDataWidget extends StatelessWidget {
  const ErrorDataWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: const [
        Icon(
          Icons.error,
          color: Colors.red,
        ),
        SizedBox(
          height: 10,
        ),
        Text('Erreur lors du chargement des données')
      ],
    );
  }
}
