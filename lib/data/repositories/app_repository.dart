import 'package:dio/dio.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

class AppRepository {
  late Dio dio;

  AppRepository() {
    dio = Dio();
    dio.options =
        BaseOptions(baseUrl: 'https://delivery-app-api-version1.herokuapp.com');
    dio.interceptors.add(
      PrettyDioLogger(
        requestBody: true,
        responseBody: true,
      ),
    );
  }
}
