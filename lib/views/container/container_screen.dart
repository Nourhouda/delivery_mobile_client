// import 'package:delivery_app/views/cart/cart_screen.dart';
// import 'package:delivery_app/views/home/home_screen.dart';
// import 'package:delivery_app/views/profile/profile_screen.dart';
// import 'package:delivery_app/views/search/search_screen.dart';
// import 'package:flutter/material.dart';

// class ContainerScreen extends StatefulWidget {
//   const ContainerScreen({Key? key}) : super(key: key);

//   @override
//   State<ContainerScreen> createState() => _ContainerScreenState();
// }

// class _ContainerScreenState extends State<ContainerScreen> {
//   int index = 0;

//   @override
//   Widget build(BuildContext context) {
//     return WillPopScope(
//       onWillPop: () async {
//         if (index == 0) {
//           return true;
//         }
//         setState(() {
//           index = 0;
//         });
//         return false;
//       },
//       child: Scaffold(
//         backgroundColor: Colors.white,
//         appBar: AppBar(
//           backgroundColor: Theme.of(context).primaryColor,
//           leading: Column(
//             mainAxisAlignment: MainAxisAlignment.center,
//             children: [
//               Text(
//                 'Marketplace',
//                 style: Theme.of(context).textTheme.headline5?.copyWith(
//                       fontWeight: FontWeight.bold,
//                       color: Colors.white,
//                     ),
//               ),
//             ],
//           ),
//           leadingWidth: 200,
//         ),
//         bottomNavigationBar: BottomNavigationBar(
//           onTap: (i) {
//             setState(() {
//               index = i;
//             });
//           },
//           currentIndex: index,
//           selectedItemColor: Colors.white,
//           unselectedItemColor: Colors.white.withOpacity(0.3),
//           items: [
//             BottomNavigationBarItem(
//               icon: const Icon(Icons.home),
//               label: 'Accueil',
//               backgroundColor: Theme.of(context).primaryColor,
//             ),
//             BottomNavigationBarItem(
//               icon: const Icon(Icons.search),
//               label: 'Recherche',
//               backgroundColor: Theme.of(context).primaryColor,
//             ),
//             BottomNavigationBarItem(
//               icon: const Icon(Icons.shopping_cart_checkout),
//               label: 'Mon panier',
//               backgroundColor: Theme.of(context).primaryColor,
//             ),
//             BottomNavigationBarItem(
//               icon: const Icon(Icons.person),
//               label: 'Mon profil',
//               backgroundColor: Theme.of(context).primaryColor,
//             ),
//           ],
//         ),
//         body: Padding(
//           padding: const EdgeInsets.symmetric(
//             vertical: 10,
//           ),
//           child: renderScreen(),
//         ),
//       ),
//     );
//   }

//   Widget renderScreen() {
//     switch (index) {
//       case 0:
//         return const HomeScreen();
//       case 1:
//         return const SearchScreen();
//       case 2:
//         return const CartScreen();
//       default:
//         return const ProfileScreen();
//     }
//   }
// }
import 'package:delivery_app/views/cart/cart_screen.dart';
import 'package:delivery_app/views/home/home_screen.dart';
import 'package:delivery_app/views/profile/profile_screen.dart';
import 'package:delivery_app/views/search/search_screen.dart';
import 'package:flutter/material.dart';

class ContainerScreen extends StatefulWidget {
  const ContainerScreen({Key? key}) : super(key: key);

  @override
  State<ContainerScreen> createState() => _ContainerScreenState();
}

class _ContainerScreenState extends State<ContainerScreen> {
  int index = 0;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (index == 0) {
          return true;
        }
        setState(() {
          index = 0;
        });
        return false;
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Theme.of(context).primaryColor,
          leading: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'Marketplace',
                style: Theme.of(context).textTheme.subtitle1?.copyWith(
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
              ),
            ],
          ),
          leadingWidth: 200,
        ),
        bottomNavigationBar: BottomNavigationBar(
          onTap: (i) {
            setState(() {
              index = i;
            });
          },
          currentIndex: index,
          selectedItemColor: Colors.white,
          unselectedItemColor: Colors.white.withOpacity(0.3),
          items: [
            BottomNavigationBarItem(
              icon: const Icon(Icons.home),
              label: 'Accueil',
              backgroundColor: Theme.of(context).primaryColor,
            ),
            BottomNavigationBarItem(
              icon: const Icon(Icons.search),
              label: 'Recherche',
              backgroundColor: Theme.of(context).primaryColor,
            ),
            BottomNavigationBarItem(
              icon: const Icon(Icons.shopping_bag),
              label: 'Mon panier',
              backgroundColor: Theme.of(context).primaryColor,
            ),
            BottomNavigationBarItem(
              icon: const Icon(Icons.person),
              label: 'Mon profil',
              backgroundColor: Theme.of(context).primaryColor,
            ),
          ],
        ),
        body: Padding(
          padding: const EdgeInsets.symmetric(
            vertical: 10,
          ),
          child: renderScreen(),
        ),
      ),
    );
  }

  Widget renderScreen() {
    switch (index) {
      case 0:
        return const HomeScreen();
      case 1:
        return const SearchScreen();
      case 2:
        return const CartScreen();
      default:
        return const ProfileScreen();
    }
  }
}
