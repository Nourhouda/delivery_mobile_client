import 'dart:convert';
import 'dart:io';

import 'package:delivery_app/const/shared_prefs_constants.dart';
import 'package:delivery_app/data/models/user_model.dart';
import 'package:delivery_app/utils/extensions.dart';
import 'package:delivery_app/views/profile/profile_screen_controller.dart';
import 'package:delivery_app/widgets/app_text_fields.dart';
import 'package:delivery_app/widgets/image_widget.dart';
import 'package:delivery_app/widgets/loading_popup.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../widgets/app_button.dart';

class ProfileInfoScreen extends StatefulWidget {
  const ProfileInfoScreen({Key? key}) : super(key: key);

  @override
  State<ProfileInfoScreen> createState() => _ProfileInfoScreenState();
}

class _ProfileInfoScreenState extends State<ProfileInfoScreen> {
  ProfileScreenController screenController = ProfileScreenController();
  UserModel? user;
  XFile? profilePhoto;

  @override
  void initState() {
    super.initState();
    _getUserInfo();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        width: MediaQuery.of(context).size.width,
        child: SafeArea(
          child: Column(
            children: [
              const SizedBox(
                height: 20,
              ),
              SizedBox(
                width: 60,
                height: 60,
                child: Stack(
                  children: [
                    profilePhoto == null
                        ? CircleAvatar(
                            backgroundImage: MemoryImage(
                              const Base64Decoder()
                                  .convert(user?.imageProfile ?? ''),
                            ),
                          )
                        : Container(
                            width: 80,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              image: DecorationImage(
                                image: FileImage(
                                  File(profilePhoto!.path),
                                ),
                              ),
                            ),
                          ),
                    Align(
                      alignment: Alignment.bottomRight,
                      child: InkWell(
                        onTap: _displayImagePickerPopup,
                        child: const Icon(
                          Icons.edit_outlined,
                        ),
                      ),
                    )
                  ],
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              Text(
                user == null ? 'N/A' : '${user!.firstname} ${user!.lastName}',
                style: Theme.of(context).textTheme.headline6?.copyWith(
                      fontWeight: FontWeight.bold,
                    ),
              ),
              Text(user == null ? 'N/A' : user!.email),
              const SizedBox(
                height: 20,
              ),
              Expanded(
                child: ListView(
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        AppTextField(
                          placeholder: 'Email',
                          controller: screenController.emailTextController,
                          enabled: false,
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        AppTextField(
                          placeholder: 'Name',
                          controller: screenController.nameTextController,
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        AppTextField(
                          placeholder: 'Lastname',
                          controller: screenController.lastnameTextController,
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        AppTextField(
                          placeholder: 'Adresse',
                          controller: screenController.adressTextController,
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        AppTextField(
                          placeholder: 'Téléphone',
                          controller: screenController.phoneTextController,
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        AppTextField(
                          placeholder: 'Ville',
                          controller: screenController.cityTextController,
                        ),
                        const SizedBox(
                          height: 50,
                        ),
                        AppButton(
                          buttonText: 'Mettre à jour',
                          onTap: _onSubmitPressed,
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _onSubmitPressed() {
    if (screenController.verifyInput()) {
      const LoadingPopup().show(context);
      screenController.updateUser().then((value) {
        Navigator.pop(context);
        FocusScope.of(context).requestFocus(FocusNode());
        if (value != null) {
          context.displaySnackbar(
            "Votre profile est mis à jour",
            Colors.green,
          );
          _updateInfo(value);
        } else {
          context.displayErrorSnackbar(
            "Veuillez verifier tout les champs obligatoire",
          );
        }
      });
    } else {
      context.displayErrorSnackbar(
        "Veuillez verifier tout les champs obligatoire",
      );
    }
  }

  _getUserInfo() async {
    var sharedPrefs = await SharedPreferences.getInstance();
    var userString = sharedPrefs.getString(userKey);
    _updateInfo(UserModel.fromJson(jsonDecode(userString ?? '')));
  }

  _updateInfo(userModel) {
    setState(() {
      user = userModel;
      if (user != null) {
        screenController.initState(user!);
      }
    });
  }

  _displayImagePickerPopup() {
    showCupertinoModalPopup(
      context: context,
      builder: (context) => CupertinoActionSheet(
        title: const Text("Profile image"),
        message: const Text("Select the source of the image"),
        actions: <Widget>[
          CupertinoActionSheetAction(
            child: const Text("Camera"),
            onPressed: () async {
              final ImagePicker _picker = ImagePicker();
              XFile? photo =
                  await _picker.pickImage(source: ImageSource.camera);
              if (photo != null) {
                setState(() {
                  profilePhoto = photo;
                  screenController.profileImage = photo;
                });
              }
            },
          ),
          CupertinoActionSheetAction(
            child: const Text("Gallery"),
            onPressed: () async {
              final ImagePicker _picker = ImagePicker();
              XFile? photo =
                  await _picker.pickImage(source: ImageSource.gallery);
              if (photo != null) {
                setState(() {
                  profilePhoto = photo;
                  screenController.profileImage = photo;
                });
              }
            },
          ),
          CupertinoActionSheetAction(
            child: const Text("Cancel"),
            isDestructiveAction: true,
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ],
      ),
    );
  }
}
