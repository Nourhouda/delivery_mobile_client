import 'dart:convert';
import 'dart:io';

import 'package:delivery_app/data/models/user_model.dart';
import 'package:delivery_app/data/repositories/auth_repository.dart';
import 'package:flutter/cupertino.dart';
import 'package:image_picker/image_picker.dart';

class ProfileScreenController {
  final TextEditingController emailTextController = TextEditingController();
  final TextEditingController nameTextController = TextEditingController();
  final TextEditingController lastnameTextController = TextEditingController();
  final TextEditingController adressTextController = TextEditingController();
  final TextEditingController phoneTextController = TextEditingController();
  final TextEditingController cityTextController = TextEditingController();
  XFile? profileImage;

  initState(UserModel userModel) {
    emailTextController.text = userModel.email;
    nameTextController.text = userModel.firstname;
    lastnameTextController.text = userModel.lastName;
    adressTextController.text = userModel.adresse;
    phoneTextController.text = userModel.numTel;
    cityTextController.text = userModel.ville;
  }

  verifyInput() {
    return emailTextController.text.isNotEmpty &&
        nameTextController.text.isNotEmpty &&
        lastnameTextController.text.isNotEmpty;
  }

  Future<UserModel?> updateUser() async {
    return await AuthRepository().updateUserProfile(
      name: nameTextController.text,
      lastname: lastnameTextController.text,
      adress: adressTextController.text,
      city: cityTextController.text,
      email: emailTextController.text,
      phone: phoneTextController.text,
      image: profileImage != null ? base64Encode(File(profileImage!.path).readAsBytesSync()) : ''
    );
  }
}
