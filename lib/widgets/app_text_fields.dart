import 'package:flutter/material.dart';

class AppTextField extends StatelessWidget {
  final String placeholder;
  final TextEditingController controller;
  final bool obscureText;
  final double height;
  final bool enabled;

  const AppTextField({
    Key? key,
    required this.placeholder,
    required this.controller,
    this.enabled = true,
    this.obscureText = false,
    this.height = 60,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 300,
      height: height,
      child: TextField(
        enabled: enabled,
        controller: controller,
        obscureText: obscureText,
        maxLines: obscureText ? 1 : 10,
        style: TextStyle(
            color: enabled ? Colors.black : Colors.black.withOpacity(0.5)),
        decoration: InputDecoration(
          label: Text(placeholder),
          fillColor: const Color(0xffF3F3F3),
          filled: true,
          enabledBorder: OutlineInputBorder(
            borderSide: enabled
                ? const BorderSide(
                    color: Color(0xff61C002),
                  )
                : const BorderSide(
                    color: Color.fromARGB(255, 106, 106, 106),
                  ),
            borderRadius: BorderRadius.circular(10),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: enabled
                ? const BorderSide(
                    color: Color(0xff61C002),
                  )
                : const BorderSide(
                    color: Color.fromARGB(255, 106, 106, 106),
                  ),
            borderRadius: BorderRadius.circular(10),
          ),
          errorBorder: OutlineInputBorder(
            borderSide: enabled
                ? const BorderSide(
                    color: Color(0xff61C002),
                  )
                : const BorderSide(
                    color: Color.fromARGB(255, 106, 106, 106),
                  ),
            borderRadius: BorderRadius.circular(10),
          ),
        ),
      ),
    );
  }
}
