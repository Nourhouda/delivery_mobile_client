import 'package:delivery_app/data/models/category_model.dart';
import 'package:delivery_app/data/repositories/app_repository.dart';

class CategoryRepository extends AppRepository {
  Future<List<CategoryModel>> getListCategories() async {
    try {
      var serverResponse = await dio.get('/api/v1/all-category');
      return List<CategoryModel>.from(
        serverResponse.data['data'].map(
          (e) => CategoryModel.fromJson(e),
        ),
      );
    } catch (e) {
      return [];
    }
  }
}
