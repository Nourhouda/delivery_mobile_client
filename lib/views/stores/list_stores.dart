import 'package:delivery_app/views/stores/list_stores_controller.dart';
import 'package:delivery_app/widgets/loading_widget.dart';
import 'package:delivery_app/widgets/store_item.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ListStoreScreen extends StatefulWidget {
  const ListStoreScreen({Key? key}) : super(key: key);

  @override
  State<ListStoreScreen> createState() => _ListStoreScreenState();
}

class _ListStoreScreenState extends State<ListStoreScreen> {
  late ListStoreController screenController;

  @override
  void initState() {
    super.initState();
    screenController = Provider.of<ListStoreController>(
      context,
      listen: false,
    );
    screenController.resetState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        title: const Text('Liste des boutiques'),
      ),
      body: Consumer<ListStoreController>(
        builder: (context, provider, child) {
          return provider.dataIsLoading
              ? Container(
                  color: Colors.white,
                  width: MediaQuery.of(context).size.width,
                  child: const LoadingWidget(),
                )
              : provider.stores.isEmpty
                  ? Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset('assets/icons/empty_cart_ill.png'),
                        const SizedBox(
                          height: 10,
                        ),
                        const Text('Aucun boutique est trouvé')
                      ],
                    )
                  : Container(
                      width: MediaQuery.of(context).size.width,
                      color: Colors.white,
                      child: ListView(
                        children: List.generate(
                          provider.stores.length,
                          (index) => Padding(
                            padding: const EdgeInsets.symmetric(
                              horizontal: 10,
                              vertical: 10,
                            ),
                            child: InkWell(
                              onTap: () {
                                Navigator.pushNamed(
                                  context,
                                  '/store-detail',
                                  arguments: provider.stores[index],
                                );
                              },
                              child: StoreItem(
                                store: provider.stores[index],
                              ),
                            ),
                          ),
                        ),
                      ),
                    );
        },
      ),
    );
  }
}
