import 'package:delivery_app/data/repositories/auth_repository.dart';
import 'package:flutter/cupertino.dart';

class LoginController {
  TextEditingController emailTextController = TextEditingController();
  TextEditingController passwordTextController = TextEditingController();

  bool verifInput() {
    return emailTextController.text.isEmpty ||
        passwordTextController.text.isEmpty;
  }

  Future<dynamic> loginUser() {
    return AuthRepository().loginUser(
      emailTextController.text.toLowerCase().replaceAll(' ', ''),
      passwordTextController.text,
    );
  }
}
