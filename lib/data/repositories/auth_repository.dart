import 'dart:convert';

import 'package:delivery_app/const/shared_prefs_constants.dart';
import 'package:delivery_app/data/models/user_model.dart';
import 'package:delivery_app/data/repositories/app_repository.dart';
import 'package:dio/dio.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthRepository extends AppRepository {
  Future<dynamic> loginUser(String email, String password) async {
    try {
      var serverResponse = await dio.post('/api/v1/login-client', data: {
        "email": email,
        "password": password,
      });
      var data = serverResponse.data;
      print("data");
      print(data['data']);
      var userModel = UserModel.fromJson(data['data']);
      print("userModel");
      print(userModel);
      (await SharedPreferences.getInstance())
        ..setString(emailKey, email)
        ..setString(passwordKey, password)
        ..setString(userKey, jsonEncode(userModel));

      return data;
    } on DioError catch (e) {
      if (e.response?.statusCode == 200) {
        var data = e.response?.data;
        var user = UserModel.fromJson(data['data']);

        (await SharedPreferences.getInstance())
          ..setString(emailKey, email)
          ..setString(passwordKey, password)
          ..setString(userKey, jsonEncode(user));
        return data;
      } else {
        print("not found");
        return null;
      }
    }
  }

  Future<UserModel?> registerUser(
    String email,
    String password,
    String name,
    String lastname,
  ) async {
    try {
      var serverResponse = await dio.post('/api/v1/add-client', data: {
        "firstname": name,
        "lastName": lastname,
        "email": email,
        "password": password,
      });
      var data = serverResponse.data;
      var userModel = UserModel.fromJson(data['data']);
      (await SharedPreferences.getInstance())
        ..setString(emailKey, email)
        ..setString(passwordKey, password)
        ..setString(userKey, jsonEncode(userModel));
      return userModel;
    } on DioError catch (e) {
      if (e.response?.statusCode == 201) {
        var data = e.response?.data;
        var userModel = UserModel.fromJson(data['data']);
        (await SharedPreferences.getInstance())
          ..setString(emailKey, email)
          ..setString(passwordKey, password)
          ..setString(userKey, jsonEncode(userModel));
        return userModel;
      } else {
        return null;
      }
    }
  }

  Future<UserModel?> updateUserProfile({
    required String name,
    required String lastname,
    required String email,
    required String adress,
    required String city,
    required String phone,
    String? image,
  }) async {
    try {
      var sharedPrefs = (await SharedPreferences.getInstance());
      var user = UserModel.fromJson(
          jsonDecode((sharedPrefs.getString(userKey)) ?? ''));
      var password = sharedPrefs.getString(passwordKey);
      var serverResponse = await dio.put('/api/v1/client/${user.id}', data: {
        "firstname": name,
        "lastName": lastname,
        "adresse": adress,
        "numTel": phone,
        "password": password,
        "email": email,
        "ville": city,
        "imageProfile": image,
      });
      var userModel = UserModel.fromJson(serverResponse.data['data']);
      sharedPrefs.setString(userKey, jsonEncode(userModel));
      return userModel;
    } catch (e) {
      return null;
    }
  }
}
