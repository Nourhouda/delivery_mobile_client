import 'package:delivery_app/data/models/order_model.dart';
import 'package:delivery_app/data/repositories/orders_repository.dart';
import 'package:flutter/cupertino.dart';

class ListOrderController extends ChangeNotifier {
  List<OrderModel> orders = [];
  bool dataIsLoading = true;

  refreshListOrders() {
    dataIsLoading = true;
    getListOrdersFromApi();
  }

  getListOrdersFromApi() {
    OrdersRepository().getListOrders().then((value) {
      orders = value;
      dataIsLoading = false;
      notifyListeners();
    });
  }
}
