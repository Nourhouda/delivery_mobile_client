import 'package:delivery_app/data/models/product_model.dart';
import 'package:delivery_app/data/repositories/product_repository.dart';
import 'package:flutter/cupertino.dart';

class SearchScreenController extends ChangeNotifier {
  final TextEditingController searchTextController = TextEditingController();
  List<ProductModel> products = [];

  Future<void> getListProducts() async {
    products = [];
    notifyListeners();
    List<ProductModel> listProducts =
        await ProductRepository().fitlerListProducts(
      searchTextController.text,
    );
    products = listProducts;
    notifyListeners();
  }
}
