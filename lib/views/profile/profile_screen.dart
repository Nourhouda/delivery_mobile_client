import 'package:cached_network_image/cached_network_image.dart';
import 'package:delivery_app/utils/utility_functions.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../data/repositories/image_repository.dart';
import '../../widgets/loading_image_header_shimmer.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  bool isConnected = false;

  @override
  void initState() {
    super.initState();
    isUserConnected().then((value) {
      setState(() {
        isConnected = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        width: MediaQuery.of(context).size.width,
        color: Colors.white,
        child: Column(
          children: [
            Stack(
              children: [
                FutureBuilder(
                  future: ImageRepository().getRandomImage(),
                  builder: (context, snapshot) {
                    if (snapshot.data == null) {
                      return Image.asset(
                        'assets/icons/store.jpg',
                        height: 200,
                        width: MediaQuery.of(context).size.width,
                        fit: BoxFit.cover,
                      );
                    }
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return const LoadingImageHeaderShimmer();
                    }
                    return CachedNetworkImage(
                      height: 200,
                      width: MediaQuery.of(context).size.width,
                      fit: BoxFit.cover,
                      imageUrl: (snapshot.data as String),
                      placeholder: (context, url) =>
                          const LoadingImageHeaderShimmer(),
                      errorWidget: (context, url, error) =>
                          const Icon(Icons.error),
                    );
                  },
                ),
                Container(
                  height: 200,
                  width: MediaQuery.of(context).size.width,
                  color: Colors.black.withOpacity(0.4),
                  child: Center(
                    child: Text(
                      'Mon compte',
                      style: Theme.of(context)
                          .textTheme
                          .headline4
                          ?.copyWith(color: Colors.white),
                    ),
                  ),
                )
              ],
            ),
            Expanded(
              child: ListView(
                children: [
                  ListTile(
                    leading: const Icon(CupertinoIcons.person),
                    trailing: const Icon(Icons.keyboard_arrow_right),
                    title: const Text('Mon compte'),
                    onTap: () {
                      if (isConnected) {
                        Navigator.pushNamed(context, '/profile-info');
                      } else {
                        Navigator.pushNamed(context, '/login').whenComplete(() async {
                          var connected = await isUserConnected();
                          if(connected){
                            Navigator.pushNamed(context, '/profile-info');
                          }
                        });
                      }
                    },
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  ListTile(
                    leading: const Icon(CupertinoIcons.list_bullet),
                    trailing: const Icon(Icons.keyboard_arrow_right),
                    title: const Text('Mes ordres'),
                    onTap: () {
                      if (isConnected) {
                        Navigator.pushNamed(context, '/orders');
                      } else {
                        Navigator.pushNamed(context, '/login').whenComplete(() async {
                          var connected = await isUserConnected();
                          if(connected){
                            Navigator.pushNamed(context, '/orders');
                          }
                        });
                      }
                    },
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  ListTile(
                    leading: const Icon(CupertinoIcons.heart),
                    trailing: const Icon(Icons.keyboard_arrow_right),
                    title: const Text('Mes favoris'),
                    onTap: () {
                      if (isConnected) {
                        Navigator.pushNamed(context, '/favoris');
                      } else {
                        Navigator.pushNamed(context, '/login').whenComplete(() async {
                          var connected = await isUserConnected();
                          if(connected){
                            Navigator.pushNamed(context, '/favoris');
                          }
                        });
                      }
                    },
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  ListTile(
                    leading: const Icon(CupertinoIcons.mail),
                    trailing: const Icon(Icons.keyboard_arrow_right),
                    title: const Text('Contactez-nous'),
                    onTap: () {
                      if (isConnected) {
                        Navigator.pushNamed(context, '/contact');
                      } else {
                        Navigator.pushNamed(context, '/login').whenComplete(() async {
                          var connected = await isUserConnected();
                          if(connected){
                            Navigator.pushNamed(context, '/contact');
                          }
                        });
                      }
                    },
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  ListTile(
                    leading: const Icon(CupertinoIcons.info),
                    trailing: const Icon(Icons.keyboard_arrow_right),
                    title: const Text('A propos'),
                    onTap: _displayAboutPopup,
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  ListTile(
                    leading: const Icon(CupertinoIcons.star),
                    trailing: const Icon(Icons.keyboard_arrow_right),
                    title: const Text('Évaluez notre application'),
                    onTap: () {},
                  ),
                  if (isConnected)
                    Column(
                      children: [
                        const SizedBox(
                          height: 15,
                        ),
                        ListTile(
                          leading: const Icon(Icons.logout),
                          trailing: const Icon(Icons.keyboard_arrow_right),
                          title: const Text('Se déconnecter'),
                          onTap: _displayLogoutPopup,
                        ),
                      ],
                    ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  void _displayAboutPopup() {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: const Text("A propos de l'application"),
          content: Container(
            color: Colors.white,
            height: 100,
            child: Column(
              children: const [
                Text('Mart place'),
                Text('Version 1.0.0'),
                Text(
                  'Conçu et développé par Magma Center',
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  void _displayLogoutPopup() {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: const Text("Déconnexion"),
          content: Text(
            'Voulez-vous vraiment déconnecter du compte ?',
            style: Theme.of(context).textTheme.caption,
          ),
          actions: [
            TextButton(
              child: Text(
                'Annuler',
                style: TextStyle(color: Colors.blue.withOpacity(0.6)),
              ),
              onPressed: () {},
            ),
            TextButton(
              child: const Text(
                'Déconnecter',
                style: TextStyle(color: Colors.red),
              ),
              onPressed: () async {
                var sharedPrefs = await SharedPreferences.getInstance();
                await sharedPrefs.clear();
                Navigator.pushReplacementNamed(context, '/login');
              },
            ),
          ],
        );
      },
    );
  }
}
