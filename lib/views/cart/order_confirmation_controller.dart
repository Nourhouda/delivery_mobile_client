import 'dart:convert';

import 'package:delivery_app/const/shared_prefs_constants.dart';
import 'package:delivery_app/data/models/cart_model.dart';
import 'package:delivery_app/data/models/order_entity.dart';
import 'package:delivery_app/data/models/user_model.dart';
import 'package:delivery_app/data/repositories/auth_repository.dart';
import 'package:delivery_app/data/repositories/orders_repository.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';

class OrderConfirmationController extends ChangeNotifier {
  List<CartModel> productsInCart = [];
  double totalAmount = 0.0;
  late UserModel userModel;

  final TextEditingController nameTextController = TextEditingController();
  final TextEditingController lastnameTextController = TextEditingController();
  final TextEditingController adressTextController = TextEditingController();
  final TextEditingController cityTextController = TextEditingController();
  final TextEditingController phoneTextController = TextEditingController();

  resetState() {
    getUserInfo();
  }

  void getListOfProductsInCart() async {
    var sharedPrefs = await SharedPreferences.getInstance();
    var cartInShared = sharedPrefs.getString(cartSharedKey);
    if (cartInShared != null) {
      productsInCart = List<CartModel>.from(
        jsonDecode(cartInShared).map(
          (e) => CartModel.fromJson(e),
        ),
      );
    }
    notifyListeners();
  }

  getUserInfo() async {
    var sharedPrefs = await SharedPreferences.getInstance();
    var userString = sharedPrefs.getString(userKey);
    userModel = UserModel.fromJson(jsonDecode(userString ?? ''));

    nameTextController.text = userModel.firstname;
    lastnameTextController.text = userModel.lastName;
    adressTextController.text = userModel.adresse;
    phoneTextController.text = userModel.numTel;
    cityTextController.text = userModel.ville;

    getListOfProductsInCart();
  }

  bool verifyInput() {
    return nameTextController.text.isNotEmpty &&
        lastnameTextController.text.isNotEmpty &&
        adressTextController.text.isNotEmpty &&
        phoneTextController.text.isNotEmpty;
  }

  onSubmitPressed(Function(bool) resultHandler) async {
    await AuthRepository().updateUserProfile(
      name: nameTextController.text,
      lastname: lastnameTextController.text,
      email: userModel.email,
      adress: adressTextController.text,
      city: cityTextController.text,
      phone: phoneTextController.text,
    );

    var userId = userModel.id;
    List<ProductByQuantity> productByQuantity = productsInCart
        .map(
          (e) => ProductByQuantity(
            product: e.product,
            quantity: e.quantity,
          ),
        )
        .toList();
    OrderEntity orderEntity = OrderEntity(
      idClient: userId,
      products: productByQuantity,
    );
    OrdersRepository().createNewOrder(orderEntity).then(resultHandler);
  }
}
