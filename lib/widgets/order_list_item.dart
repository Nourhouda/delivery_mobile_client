import 'package:delivery_app/data/models/order_model.dart';
import 'package:delivery_app/widgets/image_widget.dart';
import 'package:flutter/material.dart';

class OrderListItem extends StatelessWidget {
  final OrderModel orderModel;

  const OrderListItem({
    Key? key,
    required this.orderModel,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        ImageWidget(
          base64: orderModel.store?.imgBoutique ?? '',
          size: 50,
        ),
        const SizedBox(
          width: 5,
        ),
        Expanded(
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
              color: Colors.grey[100],
            ),
            width: MediaQuery.of(context).size.width * 0.7,
            child: Padding(
              padding: const EdgeInsets.symmetric(
                vertical: 5,
                horizontal: 10,
              ),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(orderModel.store?.adresseSiege ?? 'Adresse: N/A'),
                        const SizedBox(
                          height: 10,
                        ),
                        Text(
                          orderModel.store?.nomBoutique ?? 'Boutique: N/A',
                          style: const TextStyle(fontWeight: FontWeight.bold),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Text('Status: ${orderModel.status}'),
                      ],
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text('${orderModel.sum()} TND'),
                      Text('${orderModel.produits.length} Articles'),
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
