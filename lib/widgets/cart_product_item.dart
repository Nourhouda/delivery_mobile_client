import 'package:delivery_app/data/models/cart_model.dart';
import 'package:delivery_app/widgets/image_widget.dart';
import 'package:delivery_app/widgets/quantity_chooser.dart';
import 'package:flutter/material.dart';

class CartProductItem extends StatelessWidget {
  final CartModel cartModel;
  final Function(CartModel) onProductChanged;
  final Function(CartModel) removeProductFromCart;

  const CartProductItem({
    Key? key,
    required this.cartModel,
    required this.onProductChanged,
    required this.removeProductFromCart,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        vertical: 10,
        horizontal: 10,
      ),
      child: Row(
        children: [
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                color: Colors.grey[100],
                borderRadius: BorderRadius.circular(5),
              ),
              child: Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 10,
                  vertical: 10,
                ),
                child: Row(
                  children: [
                    ImageWidget(
                      base64: cartModel.product.img,
                      size: 60,
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          cartModel.product.nomProd,
                          style: Theme.of(context)
                              .textTheme
                              .subtitle2
                              ?.copyWith(fontWeight: FontWeight.bold),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Text(
                          'PU: ${cartModel.product.prix.toStringAsFixed(2)} DT',
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        QuantityChooser(
                          initialQuantity: cartModel.quantity,
                          onQuantityChanged: (newQuantity) {
                            onProductChanged.call(
                              CartModel(cartModel.product, newQuantity),
                            );
                          },
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
          const SizedBox(
            width: 10,
          ),
          InkWell(
            onTap: () {
              removeProductFromCart.call(cartModel);
            },
            child: Container(
              height: 80,
              width: 50,
              color: Colors.red[100],
              child: const Center(
                child: Icon(
                  Icons.delete,
                  color: Colors.red,
                  size: 30,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
