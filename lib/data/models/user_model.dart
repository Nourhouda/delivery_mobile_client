class UserModel {
  late String id;
  late String firstname;
  late String lastName;
  late String adresse;
  late String numTel;
  late bool active;
  late String email;
  late String password;
  late String role;
  late String imageProfile;
  late String ville;

  UserModel({
    required this.firstname,
    required this.lastName,
    required this.adresse,
    required this.numTel,
    required this.email,
    required this.ville,
  });

  UserModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstname = json['firstname'];
    lastName = json['lastName'];
    adresse = json['adresse'] ?? '';
    numTel = json['numTel'] ?? '';
    active = json['active'];
    email = json['email'];
    password = json['password'];
    role = json['role'] ?? '';
    imageProfile = json['imageProfile'] ?? '';
    ville = json['ville'] ?? '';
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['firstname'] = firstname;
    data['lastName'] = lastName;
    data['adresse'] = adresse;
    data['numTel'] = numTel;
    data['active'] = active;
    data['email'] = email;
    data['password'] = password;
    data['role'] = role;
    data['imageProfile'] = imageProfile;
    data['ville'] = ville;
    return data;
  }
}
