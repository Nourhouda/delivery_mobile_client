class VendeurModel {
  late String id;
  late String idBoutique;

  VendeurModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    idBoutique = json['idBoutique'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['idBoutique'] = idBoutique;
    return data;
  }
}
