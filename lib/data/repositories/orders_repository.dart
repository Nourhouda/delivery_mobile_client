import 'dart:convert';

import 'package:delivery_app/const/shared_prefs_constants.dart';
import 'package:delivery_app/data/models/order_entity.dart';
import 'package:delivery_app/data/models/order_model.dart';
import 'package:delivery_app/data/models/user_model.dart';
import 'package:delivery_app/data/repositories/app_repository.dart';
import 'package:delivery_app/data/repositories/store_repository.dart';
import 'package:shared_preferences/shared_preferences.dart';

class OrdersRepository extends AppRepository {
  Future<bool> createNewOrder(OrderEntity order) async {
    try {
      await dio.post(
        '/api/v1/commande/add',
        data: order.toJson(),
      );
      return true;
    } catch (e) {
      return false;
    }
  }

  Future<List<OrderModel>> getListOrders() async {
    try {
      var sharedPrefs = (await SharedPreferences.getInstance());
      var user = UserModel.fromJson(
        jsonDecode((sharedPrefs.getString(userKey)) ?? ''),
      );
      var serverResponse = await dio.get('/api/v1/commande/client/${user.id}');
      List<OrderModel> listOrders = [];
      for (var element in serverResponse.data['data']) {
        var store =
            await StoreRepository().getStoreBySeller(element['idVendeur']);
        listOrders.add(OrderModel.fromJson(element, store));
      }
      return listOrders;
    } catch (e) {
      return [];
    }
  }
}
