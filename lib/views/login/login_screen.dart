import 'package:delivery_app/utils/extensions.dart';
import 'package:delivery_app/views/login/login_screen_controller.dart';
import 'package:delivery_app/widgets/app_button.dart';
import 'package:delivery_app/widgets/app_text_fields.dart';
import 'package:delivery_app/widgets/loading_popup.dart';
import 'package:flutter/material.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  late LoginController screenController;

  @override
  void initState() {
    super.initState();
    screenController = LoginController();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Container(
          color: Colors.white,
          width: MediaQuery.of(context).size.width,
          child: Padding(
            padding: const EdgeInsets.all(10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Image.asset(
                      'assets/logo/logo_full.png',
                      width: 100,
                    ),
                    RichText(
                      text: const TextSpan(
                        children: [
                          TextSpan(
                            text: 'Bienvenue',
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 18,
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
                Expanded(
                  child: Align(
                    alignment: Alignment.center,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const Text(
                          "S'authentifier",
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 18),
                        ),
                        const Text(
                          'Veuillez saisir vos coordonnées',
                          style: TextStyle(
                            color: Colors.black,
                          ),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        AppTextField(
                          placeholder: 'Adresse mail',
                          controller: screenController.emailTextController,
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        AppTextField(
                          placeholder: 'Mot de passe',
                          controller: screenController.passwordTextController,
                          obscureText: true,
                        ),
                        const SizedBox(
                          height: 40,
                        ),
                        AppButton(
                          buttonText: "S'authentifier",
                          onTap: _onButtonPressed,
                        ),
                      ],
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.center,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                      vertical: 20,
                    ),
                    child: InkWell(
                      onTap: () {
                        Navigator.pushNamed(context, '/register');
                      },
                      child: RichText(
                        text: const TextSpan(
                          children: [
                            TextSpan(
                              text: "Vous n'avez pas de compte ? ",
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 14,
                              ),
                            ),
                            TextSpan(
                              text: 'créer un compte',
                              style: TextStyle(
                                color: Color(0xff61C002),
                                fontSize: 14,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  _onButtonPressed() {
    if (screenController.verifInput()) {
      context.displayErrorSnackbar(
        "Veuillez verifier tout les champs obligatoire",
      );
    } else {
      const LoadingPopup().show(context);
      screenController.loginUser().then((response) {
        Navigator.pop(context);
        if (response != null) {
          Navigator.pop(context);
        } else {
          context.displayErrorSnackbar(
            "Veuillez verifier vos coordonnées",
          );
        }
      });
    }
  }
}
