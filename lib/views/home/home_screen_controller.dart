import 'package:delivery_app/data/models/category_model.dart';
import 'package:delivery_app/data/models/product_model.dart';
import 'package:delivery_app/data/models/store_model.dart';
import 'package:delivery_app/data/repositories/category_repository.dart';
import 'package:delivery_app/data/repositories/product_repository.dart';
import 'package:delivery_app/data/repositories/store_repository.dart';
import 'package:flutter/cupertino.dart';

class HomeScreenController extends ChangeNotifier {
  List<ProductModel> products = [];
  List<CategoryModel> categories = [];
  List<StoreModel> stores = [];
  List<String> brandImages = [];

  bool dataIsLoading = true;

  initState() {
    getListCategories();
  }

  void getListCategories() {
    dataIsLoading = true;
    notifyListeners();
    CategoryRepository().getListCategories().then((value) {
      categories = value;
      getListStores();
    });
  }

  void getListStores() {
    StoreRepository().getListStores().then((value) {
      stores = value;
      brandImages = stores.map((e) => e.imgBoutique).toList();
      getListProducts();
    });
  }

  void getListProducts() {
    ProductRepository().getListProducts().then((value) {
      products = value;
      dataIsLoading = false;
      notifyListeners();
    });
  }
}
