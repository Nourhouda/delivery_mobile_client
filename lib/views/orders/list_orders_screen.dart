import 'package:delivery_app/data/models/order_model.dart';
import 'package:delivery_app/views/orders/list_order_controller.dart';
import 'package:delivery_app/widgets/loading_widget.dart';
import 'package:delivery_app/widgets/order_list_item.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:searchable_listview/searchable_listview.dart';

class ListOrderScreen extends StatefulWidget {
  const ListOrderScreen({Key? key}) : super(key: key);

  @override
  State<ListOrderScreen> createState() => _ListOrderScreenState();
}

class _ListOrderScreenState extends State<ListOrderScreen> {
  late ListOrderController orderController;

  @override
  void initState() {
    super.initState();
    orderController = Provider.of<ListOrderController>(
      context,
      listen: false,
    );
    orderController.refreshListOrders();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        title: const Text('Mes ordres'),
        leading: InkWell(
          child: const Icon(Icons.arrow_back),
          onTap: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: Container(
        color: Colors.white,
        width: MediaQuery.of(context).size.width,
        child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 10,
            vertical: 20,
          ),
          child: Consumer<ListOrderController>(
            builder: (context, provider, child) {
              return provider.dataIsLoading
                  ? const LoadingWidget()
                  : provider.orders.isEmpty
                      ? Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Image.asset('assets/icons/empty_order_img.png',width: 200,),
                            const Text('Aucune commande est trouvé')
                          ],
                        )
                      : Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Expanded(
                              child: SearchableList<OrderModel>.seperated(
                                seperatorBuilder: (p0, p1) {
                                  return const Divider(
                                    thickness: 2,
                                  );
                                },
                                initialList: provider.orders,
                                inputDecoration: InputDecoration(
                                  label: const Text('Recherche'),
                                  fillColor: const Color(0xffF3F3F3),
                                  filled: true,
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: const BorderSide(
                                      color: Color(0xff61C002),
                                    ),
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: const BorderSide(
                                      color: Color(0xff61C002),
                                    ),
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  errorBorder: OutlineInputBorder(
                                    borderSide: const BorderSide(
                                      color: Color(0xffF3F3F3),
                                    ),
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                ),
                                filter: (q) {
                                  return [];
                                },
                                builder: (item) {
                                  return Padding(
                                    padding: const EdgeInsets.symmetric(
                                      vertical: 5,
                                    ),
                                    child: InkWell(
                                      onTap: () {
                                        Navigator.pushNamed(
                                          context,
                                          '/order-detail',
                                          arguments: item,
                                        );
                                      },
                                      child: OrderListItem(
                                        orderModel: item,
                                      ),
                                    ),
                                  );
                                },
                              ),
                            )
                          ],
                        );
            },
          ),
        ),
      ),
    );
  }
}
