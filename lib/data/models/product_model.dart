class ProductModel {
  late String id;
  late String nomProd;
  late double prix;
  late String idCat;
  late String description;
  late int quantite;
  late String img;
  late String createdDate;
  late String idVendeur;

  ProductModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nomProd = json['nom_prod'];
    prix = json['prix'];
    idCat = json['id_cat'] ?? '';
    description = json['description'];
    quantite = json['quantite'] ?? 0;
    img = json['img'] ?? '';
    createdDate = json['createdDate'];
    idVendeur = json['idVendeur'] ?? '';
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['nom_prod'] = nomProd;
    data['prix'] = prix;
    data['id_cat'] = idCat;
    data['description'] = description;
    data['quantite'] = quantite;
    data['createdDate'] = createdDate;
    data['idVendeur'] = idVendeur;
    data['img'] = img;
    return data;
  }

  Map<String, dynamic> toOrderJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['nom_prod'] = nomProd;
    data['prix'] = prix;
    data['idVendeur'] = idVendeur;
    return data;
  }
}
