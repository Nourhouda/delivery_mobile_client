class StoreModel {
  late String id;
  late String nomBoutique;
  late String matricule;
  late String nomResponsable;
  late String telResponsable;
  late String imgBoutique;
  late String adresseSiege;

  StoreModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nomBoutique = json['nomBoutique'];
    matricule = json['matricule'];
    nomResponsable = json['nomResponsable'];
    telResponsable = json['telResponsable'];
    imgBoutique = json['imgBoutique'] ?? '';
    adresseSiege = json['adresseSiege'] ?? '';
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['nomBoutique'] = nomBoutique;
    data['matricule'] = matricule;
    data['nomResponsable'] = nomResponsable;
    data['telResponsable'] = telResponsable;
    data['imgBoutique'] = imgBoutique;
    data['adresseSiege'] = adresseSiege;
    return data;
  }
}
