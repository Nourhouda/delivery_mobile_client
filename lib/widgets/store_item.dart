import 'package:delivery_app/data/models/store_model.dart';
import 'package:delivery_app/widgets/image_widget.dart';
import 'package:flutter/material.dart';

class StoreItem extends StatelessWidget {
  final StoreModel store;

  const StoreItem({
    Key? key,
    required this.store,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        border: Border.all(width: 1, color: Colors.grey),
        color: Colors.grey[200],
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 10,
          vertical: 20,
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            ImageWidget(
              base64: store.imgBoutique,
              size: 60,
            ),
            const SizedBox(
              width: 10,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    store.nomBoutique,
                    style: const TextStyle(fontWeight: FontWeight.bold),
                  ),
                  Text(
                    store.adresseSiege.isNotEmpty
                        ? store.adresseSiege
                        : 'Adresse: N/A',
                    maxLines: 2,
                    textAlign: TextAlign.start,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
