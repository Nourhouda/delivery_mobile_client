import 'package:delivery_app/data/models/store_model.dart';

class OrderModel {
  late String id;
  String? idLivreur;
  String? idVendeur;
  late StoreModel? store;
  late String status;
  late List<Produits> produits;
  late String createdDate;

  OrderModel.fromJson(Map<String, dynamic> json, StoreModel? storeModel) {
    id = json['id'];
    idLivreur = json['idLivreur'];
    idVendeur = json['idVendeur'];
    store = storeModel;
    status = json['status'];
    produits = <Produits>[];
    json['produits'].forEach((v) {
      produits.add(Produits.fromJson(v));
    });
    createdDate = json['createdDate'];
  }

  double sum() {
    double somme = 0;
    produits.every((element) {
      somme += (element.quantity ?? 1) * (element.produit?.prix ?? 0);
      return true;
    });
    return somme;
  }
}

class Produits {
  Produit? produit;
  int? quantity;

  Produits({this.produit, this.quantity});

  Produits.fromJson(Map<String, dynamic> json) {
    produit =
        json['produit'] != null ? Produit.fromJson(json['produit']) : null;
    quantity = json['quantity'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    if (produit != null) {
      data['produit'] = produit!.toJson();
    }
    data['quantity'] = quantity;
    return data;
  }
}

class Produit {
  late String id;
  late String nomProd;
  late double prix;

  Produit.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nomProd = json['nom_prod'];
    prix = json['prix'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['nom_prod'] = nomProd;
    data['prix'] = prix;
    return data;
  }
}
