import 'package:flutter/material.dart';

extension BuildContextExtension on BuildContext {
  void displayErrorSnackbar(String message) {
    ScaffoldMessenger.of(this).showSnackBar(SnackBar(
      content: Text(message),
      backgroundColor: Colors.red,
    ));
  }

  void displaySnackbar(String message, Color color) {
    ScaffoldMessenger.of(this).showSnackBar(SnackBar(
      content: Text(message),
      backgroundColor: color,
    ));
  }
}
