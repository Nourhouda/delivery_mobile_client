class CategoryModel {
  late String id;
  late String nomCat;
  late String description;
  late String createdDate;
  late String icon;

  CategoryModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nomCat = json['nom_cat'];
    description = json['description'];
    createdDate = json['createdDate'];
    icon = json['img'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['nom_cat'] = nomCat;
    data['description'] = description;
    data['createdDate'] = createdDate;
    data['img'] = icon;
    return data;
  }
}
