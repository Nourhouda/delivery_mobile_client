import 'dart:convert';

import 'package:delivery_app/const/shared_prefs_constants.dart';
import 'package:delivery_app/data/models/cart_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CartScreenController extends ChangeNotifier {
  List<CartModel> productsInCart = [];
  double totalAmount = 0.0;

  void getListOfProductsInCart() async {
    productsInCart = [];
    totalAmount = 0.0;
    var sharedPrefs = await SharedPreferences.getInstance();
    var cartInShared = sharedPrefs.getString(cartSharedKey);
    if (cartInShared != null) {
      productsInCart = List<CartModel>.from(
        jsonDecode(cartInShared).map(
          (e) => CartModel.fromJson(e),
        ),
      );
    }
    _updateTotalAmount();
  }

  void onProductChanged(CartModel cartModel) async {
    var sharedPrefs = await SharedPreferences.getInstance();
    var cartInShared = sharedPrefs.getString(cartSharedKey);
    if (cartInShared != null) {
      productsInCart = List<CartModel>.from(
        jsonDecode(cartInShared).map(
          (e) => CartModel.fromJson(e),
        ),
      );
      var index = productsInCart.indexWhere(
        (element) => element.product.id == cartModel.product.id,
      );
      productsInCart[index] = cartModel;
      sharedPrefs.setString(cartSharedKey, jsonEncode(productsInCart));
    }
    _updateTotalAmount();
  }

  void onProductRemoved(CartModel cartModel) async {
    var sharedPrefs = await SharedPreferences.getInstance();
    var cartInShared = sharedPrefs.getString(cartSharedKey);
    if (cartInShared != null) {
      productsInCart = List<CartModel>.from(
        jsonDecode(cartInShared).map(
          (e) => CartModel.fromJson(e),
        ),
      );
      productsInCart.removeWhere(
        (element) => element.product.id == cartModel.product.id,
      );
      sharedPrefs.setString(cartSharedKey, jsonEncode(productsInCart));
    }
    _updateTotalAmount();
  }

  void _updateTotalAmount() {
    totalAmount = 0.0;
    for (var element in productsInCart) {
      totalAmount += (element.product.prix * element.quantity);
    }
    notifyListeners();
  }
}
