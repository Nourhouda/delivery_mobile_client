import 'package:delivery_app/data/models/product_model.dart';

class OrderEntity {
  String idClient;
  List<ProductByQuantity> products;

  OrderEntity({
    required this.idClient,
    required this.products,
  });

  Map<String, dynamic> toJson() {
    return {
      'status': 'new',
      'idClient': idClient,
      'produits': products.map((item) => item.toJson()).toList(),
    };
  }
}

class ProductByQuantity {
  ProductModel product;
  int quantity;
  ProductByQuantity({
    required this.product,
    required this.quantity,
  });

  Map<String, dynamic> toJson() {
    return {
      'produit': product.toOrderJson(),
      'quantity': quantity,
    };
  }
}
